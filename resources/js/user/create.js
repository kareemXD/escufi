const buttonVerify = document.querySelector('[data-verify-user-type]');
const routeVeryfy =  buttonVerify.getAttribute('data-verify-user-type');
const verifyUser = async (route, params) => {
    const settings = {
        method: 'POST',
        headers: {
            'x-csrf-token': _token,
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(params)
    };
    try {
        const fetchResponse = await fetch(route, settings);
        const data = await fetchResponse.json();
        if(fetchResponse.status === 422){
          checkInfoData({}, false);
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: data.errors.matricula[0],
          });
          checkInfoData({}, false);
          return;
        }else if(fetchResponse.status === 200 || fetchResponse.status== 201){
          checkInfoData(data, true);
          return data;
        }else{
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'La matricula ingresada no es válida',
          });
          checkInfoData({}, false);
        }
        return data;
    } catch (error) {
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'La matricula ingresada no es válida',
      });
      checkInfoData({}, false);
    }    
}
const checkInfoData = (user_data = {}, callback_status= false) => {
  console.log(user_data);
  window.user_data = user_data;

    let user_validation_container = document.querySelector("#user_validation_container");
    if(callback_status){
      if(user_validation_container.classList.contains('d-none')){
        user_validation_container.classList.remove('d-none');
      }
      if(user_data.length > 0 || Object.keys(user_data).length > 0){
        document.querySelector("input[name='name']").value = user_data.nombre +' '+user_data.paterno;
        if(user_data.student_contact != undefined){
          document.querySelector("input[name='email']").value = user_data.student_contact.correo;
        }else{
          document.querySelector("input[name='email']").value = user_data.email;
        }
      }
    }else{
      if(!user_validation_container.classList.contains('d-none')){
        user_validation_container.classList.add('d-none');
      }
    }
  }

buttonVerify.addEventListener('click', () => {
  let matricula = document.querySelector("input[name='matricula']").value;
  let user_type = document.querySelector("select[name='type-user']").value;
  if(matricula == ""){
    Swal.fire({
      icon: 'error',
      title: 'Error',
      text: 'La matricula ingresada no es válida',
    });
    checkInfoData({}, false);
    return;
  }
  verifyUser(routeVeryfy, { matricula: matricula, 'type-user': user_type });
});


document.querySelector("#form_user_create").addEventListener('submit', function (event) {
  let button = this.querySelector('button[type="submit"]');
  blockButton(button);

  if (!this.checkValidity()) {
    event.preventDefault()
    event.stopPropagation()
  }
  this.classList.add('was-validated');

}, false);
