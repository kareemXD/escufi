window._ = require('lodash');


require('bootstrap');
window.moment = require('moment');
window.Swal = require('sweetalert2');
/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

window.blockButton = function(button){
    button_html = button.innerHTML;
    button.innerHTML = `<div class="spinner-border text-light" role="status">
      <span class="sr-only">Loading...</span>
    </div>`;
    button.setAttribute("disabled", "disabled");
    setTimeout(() => {
      button.innerHTML = button_html;
      button.removeAttribute("disabled");
    }, 1000);
}

window._token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

window.deleteParentButton = (buttonTarget) => {
  buttonTarget.parentNode.parentNode.remove();
}

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo';

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     forceTLS: true
// });
