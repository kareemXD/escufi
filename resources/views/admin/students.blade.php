@extends('layouts.dashboard')
@section('breadcrumb')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">Dashboard</h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">
              Administración de estudiantes
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
@endsection
@section('content')
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title mb-0">Listado de estudiantes activos para Kardex</h5>
                </div>
                <div class="">
                    <table class="table" id="students_list">
                        <thead>
                            <tr class="table table-dark text-white">
                                <th>Matricula</th>
                                <th>Paterno</th>
                                <th>Materno</th>
                                <th>Nombre</th>
                                <th>Carrera</th>
                                <th>Semestre</th>
                                <th>Kardex</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($students as $student)
                                <tr>
                                    <td>{{ $student->matricula_alumno }}</td>
                                    <td>{{ $student->paterno }}</td>
                                    <td>{{ $student->materno }}</td>
                                    <td>{{ $student->nombre }}</td>
                                    <td>{{ $student->career->name }}</td>
                                    <td>{{ $student->semester->semestre }}</td>
                                    <td><a href="{{ route('admin.kardex.create', $student) }}">Kardex</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
         
        </div>
    </div>

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
@endsection
@section('js')
<script src="{{asset('assets/extra-libs/multicheck/datatable-checkbox-init.js')}}"></script>
<script src="{{asset('assets/extra-libs/multicheck/jquery.multicheck.js')}}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.js"></script>
<script type="text/javascript">
    $(function () {        
      var table = $('#students_list').DataTable({
          pagingType: "full_numbers",
          pageLength: 25,
          dom: 'flptrip',
          processing: true,
          serverSide: true,
          ajax: "{{ route('admin.students') }}",
          columns: [
              {data: 'matricula_alumno', name: 'matricula_alumno'},
              {data: 'paterno', name: 'paterno'},
              {data: 'materno', name: 'materno'},
              {data: 'nombre', name: 'nombre'},
              {data: 'career.name', name: 'career.name'},
              {data: 'semester.semestre', name: 'semester.semestre'},
              {data: 'action', name: 'action', orderable: false, searchable: false},
          ],
          language: LNG_ES
      });
      
        
    });
  </script>
@endsection
@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.css"/>

@endsection