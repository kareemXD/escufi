@extends('layouts.dashboard')
@section('breadcrumb')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">Dashboard</h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">
              Administración de profesores
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
@endsection
@section('content')
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title mb-0">Listado de Educadores</h5>
                </div>
                <div class="">
                    <div class="d-flex">
                        <a href="{{ route('admin.professors.create') }}" class="btn btn-primary mx-1"><i class="mdi mdi-account-plus"></i></a>
                    </div>
                    <table class="table" id="professor_table">
                        <thead>
                            <tr class="table table-dark text-white">
                                <th>Matricula</th>
                                <th>Paterno</th>
                                <th>Materno</th>
                                <th>Nombre</th>
                                <th>Email</th>
                                <th>NSS</th>
                                <th>Estatus</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($professors as $professor)
                                <tr>
                                    <td>{{ $professor->matricula_professor }}</td>
                                    <td>{{ $professor->paterno }}</td>
                                    <td>{{ $professor->materno }}</td>
                                    <td>{{ $professor->nombre }}</td>
                                    <td>{{ $professor->email }}</td>
                                    <td>{{ $professor->NSS }}</td>
                                    <td>
                                        @can('all')
                                        <div class="form-check form-switch">
                                          <input onchange="return;" class="form-check-input" type="checkbox" id="flexSwitchCheckChecked{{ $professor->id }}" {{  $professor->active? 'checked' : ''  }}>
                                          <label class="form-check-label" for="flexSwitchCheckChecked{{ $professor->id }}"></label>
                                        </div>
                                        @endcan
                                    </td>
                                    <td>
                                      <a class="btn btn-primary" href="{{ route('admin.professors.edit', $professor) }}"><i class="mdi mdi-account-edit text-white"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
         
        </div>
    </div>

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
@endsection
@section('js')
<script src="{{asset('assets/extra-libs/multicheck/datatable-checkbox-init.js')}}"></script>
<script src="{{asset('assets/extra-libs/multicheck/jquery.multicheck.js')}}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.js"></script>
<script type="text/javascript">
    $(function () {        
      var table = $('#professor_table').DataTable({
          pagingType: "full_numbers",
          pageLength: 25,
          dom: 'flptrip',
          language: LNG_ES
      });
      
        
    });
  </script>
@endsection
@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.css"/>

@endsection