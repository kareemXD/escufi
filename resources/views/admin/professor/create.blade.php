@extends('layouts.dashboard')
@section('breadcrumb')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">Dashboard</h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ route('admin.professors.index') }}">Profesores</a></li>
            <li class="breadcrumb-item active" aria-current="page">
              Registrar profesor
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
@endsection
@section('content')
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title mb-0">Registrar nuevo profesor</h5>
                    <form  method="post" id="form_professor_create" class="row g-3 needs-validation" novalidate  action="{{ route('admin.professors.store') }}" >
                        @if (session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                          {{ session('success') }}
                          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                        @endif
                        @if ($errors->any())
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                          {{ session()->get('errors')->first() }}
                          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                        @endif
                        {{ csrf_field() }}
                        <hr>
                        <div class="row">
                        <div class="col-md-4">
                            <label for="paterno" class="control-label">Apellido paterno</label>
                            <input   
                            value="{{ old('paterno') }}" autocomplete="off" type="text" class="form-control @if($errors->has('paterno'))
                            is-invalid
                            @endif " name="paterno" id="paterno" required="required" minlength="3" maxlength="100">
                            <div class="invalid-feedback">
                            
                            @if($errors->has('paterno'))
                                {{ $errors->first('paterno') }}
                            @else
                            El apellido paterno es requerido
                            @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label for="materno" class="control-label">Apellido materno</label>
                            <input  value="{{ old('materno') }}"  autocomplete="off" type="text" class="form-control @if($errors->has('materno'))
                            is-invalid
                            @endif" name="materno" id="materno" required="required" minlength="3" maxlength="100">
                            <div class="invalid-feedback">
                            
                            @if($errors->has('materno'))
                                {{ $errors->first('materno') }}
                            @else
                            El apellido materno es requerido
                            @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label for="nombre" class="control-label">Nombre</label>
                            <input value="{{ old('nombre') }}" autocomplete="off" type="text" class="form-control @if($errors->has('nombre'))
                            is-invalid
                            @endif" name="nombre" id="nombre" required="required" minlength="3" maxlength="100">
                            <div class="invalid-feedback">
                            
                            @if($errors->has('nombre'))
                                {{ $errors->first('nombre') }}
                            @else
                            El nombre es requerido
                            @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="email" class="control-label">Correo Electrónico</label>
                            <input value="{{ old('email') }}" autocomplete="off" type="email" class="form-control @if($errors->has('email'))
                            is-invalid
                            @endif" name="email" id="email" required="required" minlength="3" maxlength="100">
                            <div class="invalid-feedback">
                            @if($errors->has('email'))
                            {{ $errors->first('email') }}
                            @else
                            El email ingresado no es válido
                            @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="nss" class="control-label">Numero de seguro social</label>
                            <input value="{{ old('nss') }}" autocomplete="off" type="text" class="form-control @if($errors->has('nss'))
                            is-invalid
                            @endif" name="nss" id="nss" minlength="3" maxlength="12">
                            <div class="invalid-feedback">
                            @if($errors->has('nss'))
                            {{ $errors->first('nss') }}
                            @else
                            El NSS ingresado no es valido
                            @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="date" class="control-label">Fecha de nacimiento</label>
                            <div class="input-group date" id="datepicker">
                            <input autocomplete="off" value="{{ old('fecha-nacimiento') }}" required name="fecha-nacimiento" type="text" class="form-control @if($errors->has('fecha-nacimiento'))
                            is-invalid
                            @endif" id="date"/>
                            <span class="input-group-append">
                                <span class="input-group-text bg-light d-block">
                                <i class="fa fa-calendar"></i>
                                </span>
                            </span>
                            <div class="invalid-feedback">
                                @if($errors->has('fecha-nacimiento'))
                                {{ $errors->first('fecha-nacimiento') }}
                                @else
                                Ingrese una fecha válida
                                @endif
                            </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="curp" class="control-label">CURP</label>
                            <input value="{{ old('curp') }}" autocomplete="off" type="text" class="form-control @if($errors->has('curp'))
                            is-invalid
                            @endif" name="curp" id="curp" minlength="3" maxlength="18">
                            <div class="invalid-feedback">
                            @if($errors->has('curp'))
                            {{ $errors->first('curp') }}
                            @else
                            El NSS ingresado no es valido
                            @endif
                            </div>
                        </div>
                        <div class="d-grid gap-2 mt-3">
                            <button type="submit" class="btn btn-info text-white" type="button" >Registrar</button>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
         
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
@endsection
@section('js')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $.fn.datepicker.dates['es'] = {
		days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
		daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
		daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
		months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
		monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
		today: "Hoy",
		monthsTitle: "Meses",
		clear: "Borrar",
		weekStart: 1,
		format: "yyyy-mm-dd"
	};
    document.querySelector("#form_professor_create").addEventListener('submit', function (event) {
        let button = this.querySelector('button[type="submit"]');
        blockButton(button);
      
        if (!this.checkValidity()) {
          event.preventDefault()
          event.stopPropagation()
        }
        this.classList.add('was-validated');
      
      }, false);
      $('#datepicker').datepicker( {language: 'es'});
</script>
@endsection
