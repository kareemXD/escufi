@extends('layouts.dashboard')
@section('breadcrumb')
<style>
    @media (max-width: 767px) {
        .table-responsive .dropdown-menu {
            position: static !important;
        }
    }
    @media (min-width: 768px) {
        .table-responsive {
            overflow: inherit;
        }
    }
</style>
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">Solicitud {{ $kardexConfirmation->request_number }}</h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">
                <a href="{{ route('admin.kardex.qualification-requests') }}">Solicitudes de kardex</a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">
                Solicitud número {{ $kardexConfirmation->request_number }}
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
@endsection
@section('content')
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title mb-0">Matricula: {{ $kardexConfirmation->professor->matricula_professor }}</h5>
                    <h6 class="card-title mb-0">Nombre: {{ $kardexConfirmation->professor->full_name }}</h6>
                    <div class="d-flex flex-row-reverse"><button class="btn btn-info text-white">Documentos</button></div>
                    <hr>
                    <div class="row">
                        <form id="form_confirmation_kardex" method="post" action="{{ route('admin.kardex.qualification-requests-kardex', $kardexConfirmation) }}">
                            {{ csrf_field() }}
                            <table class="table" id="kardex_table">
                                <thead>
                                    <tr >
                                        <th class="text-nowrap">
                                            <div class="form-check form-switch">
                                                <label class="form-check-label" for="confirmQualificationCheck"></label><input class="form-check-input" type="checkbox" id="confirmQualificationCheck">
                                            </div>
                                        </th>
                                        <th>Asignatura</th>
                                        <th>Alumno</th>
                                        <th>Matricula</th>
                                        <th>Carrera</th>
                                        <th>Semestre</th>
                                        <th>Minimo aprobatorio</th>
                                        <th>Pre Calificación</th>
                                        <th>Calificación</th>
                                        <th>Estatus</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($kardexConfirmation->kardexs as $index => $kardex)
                                        <tr>
                                            <td class="d-flex">
                                                <div class="form-check form-switch">
                                                    <label data-id-kardex="{{ $kardex->id }}" class="form-check-label" for="kardexInput{{ $kardex->id }}"></label><input name="is-confirmed[]" 
                                                    value="{{$kardex->id}}" class="form-check-input is-confirmed-check" type="checkbox" id="kardexInput{{ $kardex->id }}">
                                                </div>
                                            </td>
                                            <td>{{ $kardex->signature->asignatura }}</td>
                                            <td>{{ $kardex->student->full_name }}</td>
                                            <td>{{ $kardex->student->matricula_alumno }}</td>
                                            <td>{{ $kardex->student->career->name }}</td>
                                            <td>{{ $kardex->semester->semestre }}</td>
                                            <td>{{ number_format($kardex->approved_minimum, 2) }}</td>
                                            <td>{{ number_format($kardex->prequalification,2) }}</td>
                                            <td class="qualification">{{ number_format($kardex->qualification,2) }}</td>
                                            <td class="qualification-container d-none"><input name="qualification_{{$kardex->id}}" type="number" class="form-control" class="qualification_input" step="0.01" value="{{ number_format($kardex->qualification,2) }}"></td>
                                            <td>{{ $kardex->status_kardex }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <th>
                                        <div class="btn-group">
                                            <button data-boundary="viewport" type="button" class="btn btn-danger dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                                            Action
                                            </button>
                                            <ul class="dropdown-menu">
                                            <li><input name="kardex-qualification-status" type="submit" class="dropdown-item" value="Confirmado"></li>
                                            <li><input name="kardex-qualification-status" type="submit" class="dropdown-item" value="Rechazado"></li>
                                            </ul>
                                        </div>
                                    </th>
                                    <th colspan="9"></th>
                                    <th><button class="btn btn-primary">Confirmar</button></th>
                                </tfoot>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
@endsection
@section('js')
<script src="{{asset('assets/extra-libs/multicheck/datatable-checkbox-init.js')}}"></script>
<script src="{{asset('assets/extra-libs/multicheck/jquery.multicheck.js')}}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<!-- Popperjs -->
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.2/dist/umd/popper.min.js"
      crossorigin="anonymous"></script>
<!-- Tempus Dominus JavaScript -->
<script src="https://cdn.jsdelivr.net/gh/Eonasdan/tempus-dominus@master/dist/js/tempus-dominus.js"
      crossorigin="anonymous"></script>
<script type="text/javascript" >
    /**listeners**/
    document.querySelector("#confirmQualificationCheck").addEventListener("change", function(e) {
        document.querySelectorAll(".is-confirmed-check").forEach((element) => {
            element.checked = this.checked;
        });
    });
    /**listener to check if any input is checked check the global checked***/
    document.querySelectorAll(".is-confirmed-check").forEach((element) => {
        element.addEventListener("change", function(e){
            let countCheckeds = 0;
            if(element.checked === false){
                document.querySelector("#confirmQualificationCheck").checked = element.checked;
                return;
            }
            document.querySelectorAll(".is-confirmed-check").forEach((el) => {
                if(el.checked === false){
                    countCheckeds++;
                }
            });
            document.querySelector("#confirmQualificationCheck").checked = (countCheckeds === 0);

        });
    });

    /**listener to confirm o reject request***/
    document.getElementById("form_confirmation_kardex").addEventListener("submit", function(e){
        
        e.preventDefault();
        
    });

    /***dropzone for files**/
    //#todo review files https://docs.dropzone.dev/configuration/basics
</script>

@endsection
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.2.0/dist/select2-bootstrap-5-theme.min.css" />
<!-- Or for RTL support -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.css"/>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.2.0/dist/select2-bootstrap-5-theme.rtl.min.css" />
<!-- Tempus Dominus Styles -->
<link href="https://cdn.jsdelivr.net/gh/Eonasdan/tempus-dominus@master/dist/css/tempus-dominus.css"
      rel="stylesheet" crossorigin="anonymous">

@endsection