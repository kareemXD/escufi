@extends('layouts.dashboard')
@section('breadcrumb')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">Dashboard</h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">
              Kardex
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
@endsection
@section('content')
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title mb-0">Listado de Kardex </h5>
                </div>
                <div class="">
                  @if($errors->any())
                      {!! implode('', $errors->all('<div>:message</div>')) !!}
                  @endif
                  <form action="">
                    <div class="row">
                      <div class="col-md-3 m-2">
                        <label for="search_period">Periodo</label>
                        <select name="search-period" id="search_period" class="form-select">
                            @foreach($schoolPeriods as $schoolP)
                            <option @if(request()->{"search-period"} == $schoolP->code) selected @endif value="{{ $schoolP->code }}">{{$schoolP->code}}</option>
                            @endforeach
                        </select>
                      </div>
                      <div class="col-md-3 m-2">
                        <label for="search_professors">Docentes</label>
                        <select name="search-professors" class="form-select" name="" id="search_professors">
                          @foreach($professors as $professor)
                            <option @if(request()->{"search-professors"} == $professor->matricula_professor) selected @endif value="{{ $professor->matricula_professor }}">{{ $professor->full_name }}</option>
                          @endforeach
                        </select>
                    </div>
                    <div class="col-md-3 d-flex align-items-end bd-highligh mb-2">
                      <button type="submit" class="btn btn-primary" >Buscar</button>
                    </div>
                  </form>
                    </div>
                    <table class="table" id="kardex_table">
                        <thead>
                            <tr class="table table-dark text-white">
                                <th>Asignatura</th>
                                <th>Alumno</th>
                                <th>Matricula</th>
                                <th>Carrera</th>
                                <th>Semestre</th>
                                <th>Minimo aprobatorio</th>
                                <th>Pre Calificación</th>
                                <th>Calificación</th>
                                <th>Estatus</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($kardexs as $kardex)
                                <tr>

                                    <td>{{ $kardex->signature->asignatura }}</td>
                                    <td>{{ $kardex->student->full_name }}</td>
                                    <td>{{ $kardex->student->matricula_alumno }}</td>
                                    <td>{{ $kardex->student->career->name }}</td>
                                    <td>{{ $kardex->semester->semestre }}</td>
                                    <td>{{ number_format($kardex->approved_minimum, 2) }}</td>
                                    <td>{{ number_format($kardex->prequalification,2) }}</td>
                                    <td class="qualification">{{ number_format($kardex->qualification,2) }}</td>
                                    <td class="qualification-container d-none"><input name="qualification_{{$kardex->id}}" type="number" class="form-control" class="qualification_input" step="0.01" value="{{ number_format($kardex->qualification,2) }}"></td>
                                    <td>{{ $kardex->status_kardex }}</td>
                                    <td class="d-flex">
                                      @if($kardex->kardex_qualification_status == null ||  $kardex->kardex_qualification_status=="rechazado")
                                      <button  onclick="showMessageKardex('Aun en curso!')" type="button" class="btn btn-info" ><i class="fa  fa-school text-white"></i></button>
                                      @elseif($kardex->kardex_qualification_status == "revision")
                                      <button data-bs-toggle="modal" data-bs-target="#modalConfirm" data-kardex="{{ json_encode($kardex) }}" data-value="confirmado" data-save="{{ route('admin.kardex.confirm-qualification', $kardex) }}" type="button" class="btn btn-primary" > <i class="fa fa-envelope-circle-check text-white"></i></button>
                                      <button data-bs-toggle="modal" data-bs-target="#modalConfirm" data-save="{{ route('admin.kardex.confirm-qualification', $kardex) }}" data-value="rechazado" type="button" class="btn btn-danger" > <i class="fa fa-times-circle text-white"></i></button>
                                      @else
                                      <button onclick="showMessageKardex('calificación confirmada')" type="button" class="btn btn-info" ><i class="fa fa-check text-white"></i></button>
                                      @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
         
        </div>
    </div>

</div>
@include('partials.modals.confirm-kardex-qualification')
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
@endsection
@section('js')
<script src="{{asset('assets/extra-libs/multicheck/datatable-checkbox-init.js')}}"></script>
<script src="{{asset('assets/extra-libs/multicheck/jquery.multicheck.js')}}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/i18n/es.js"></script>

<script type="text/javascript">
    @if(session()->has('success'))
      Swal.fire('{{ session()->get("success") }}');

    @endif


    function showMessageKardex(message){
      Swal.fire(message);
    }

  </script>
  
@endsection
@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.css"/>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" />

@endsection