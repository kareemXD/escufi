@extends('layouts.dashboard')
@section('breadcrumb')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">Dashboard</h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">
              Administración de kardex
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
@endsection
@section('content')
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title mb-0">Matricula: {{ $student->matricula_alumno }}</h5>
                    <h6 class="card-title mb-0">Nombre: {{ $student->full_name }}</h6>
                    <h6 class="card-title mb-0">CARRERA: {{ strtoupper($student->career->name) }}</h6>
                    <h6 class="card-title mb-0">SEMESTRE {{ strtoupper( $student->semester->semestre) }}</h6>
                    @if(session()->has("success"))
                    <div class="alert alert-primary" role="alert">
                       {{ session()->get('success') }}
                      </div>
                    @endif
                    @if($errors->any())
                      <div class="alert alert-danger" role="alert">
                         {{ session()->get('errors')->first();}}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-md-5">
                            <label for="school_period"></label>
                            <select class="form-select" name="school-period" id="school_period">
                                @foreach($schoolPeriods as $schoolPeriod)
                                <option value="{{ $schoolPeriod->id }}">{{ $schoolPeriod->code }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="mx-3">
                  @if($student->hasKardex())
                    <table class="table" id="school-offer-list">
                        <thead>
                            <tr class="table table-dark text-white">
                                <th>Asignatura</th>
                                <th>Profesor</th>
                                <th>Matricula profesor</th>
                                <th>Minimo aprobatorio</th>
                                <th>Creditos</th>
                                <th>Estatus</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            @foreach($kardexs as $kardex)
                            <tr>    
                                <td>{{$kardex->signature->asignatura}} <input name="school-offer[]" type="hidden" value="{{ $kardex->id }}"></td>
                                <td>{{ $kardex->professor->full_name }}</td>
                                <td>{{ $kardex->matricula_professor}}</td>
                                <td>{{ number_format($kardex->approved_minimum, 2) }}</td>
                                <td>{{ $kardex->credits }}</td>
                                <td>{{ $kardex->status_kardex }}</td>
                                <td><button class="btn btn-primary" ><i class="fa fa-pen"></i></button></td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>

                    @else
                    <form id="addKardexPeriod" action="{{ route('admin.kardex.store', $student) }}" method="post">
                      {{ csrf_field() }}
                      <input type="hidden" name="kardex-school-period" value="{{ $schoolPeriods[0]->code }}">
                      <table  class="table" id="school-offer-list">
                        <thead>
                          <tr class="table table-dark text-white">
                            <th>Carrera</th>
                            <th>Asignatura</th>
                            <th>Profesor</th>
                            <th>Matricula profesor</th>
                            <th>Minimo aprobatorio</th>
                            <th>Creditos</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                          @foreach($schoolOffers as $schoolOffer)
                          <tr>
                              <td>{{$schoolOffer->career->name}}<input name="school-offer[]" type="hidden" value="{{ $schoolOffer->id }}"></td>
                              <td>{{$schoolOffer->signature->asignatura}}</td>
                              <td>{{ $schoolOffer->professor->full_name }}</td>
                              <td>{{ $schoolOffer->matricula_professor}}</td>
                              <td>{{ number_format($schoolOffer->approving_minimum, 2)}}</td>
                              <td>{{ $schoolOffer->credits }}</td>
                              <td><button onclick="deleteKardexElement(this)" class="btn btn-danger" type="button"  ><i class="fa fa-remove"></i></button></td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </form>
                    @endif
                    @if(!$student->hasKardex())
                    <div class="d-grid gap-2 col-6 mx-auto mb-2">
                        <button onclick="confirmKardexData()" class="btn btn-secondary">Confirmar Kardex <svg style="width:24px;height:24px" viewBox="0 0 24 24">
                            <path fill="currentColor" d="M10,17L5,12L6.41,10.58L10,14.17L17.59,6.58L19,8M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2Z" />
                        </svg></button>
                    </div>
                    
                    @endif
                </div>
            </div>
         
        </div>
    </div>

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
@endsection
@section('js')
<script src="{{asset('assets/extra-libs/multicheck/datatable-checkbox-init.js')}}"></script>
<script src="{{asset('assets/extra-libs/multicheck/jquery.multicheck.js')}}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<!-- Popperjs -->
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.2/dist/umd/popper.min.js"
      crossorigin="anonymous"></script>
<!-- Tempus Dominus JavaScript -->
<script src="https://cdn.jsdelivr.net/gh/Eonasdan/tempus-dominus@master/dist/js/tempus-dominus.js"
      crossorigin="anonymous"></script>
<script type="text/javascript">
    @if(!$student->hasKardex())
    $(function(){
        Swal.fire({
            icon: 'info',
            title: 'Alerta',
            text: 'Este alumno aun no cuenta con el kardex del periodo',
          });
    });
    const deleteKardexElement = (element) => {
        Swal.fire({
            title: '¿Estas seguro de eliminar esta materia?',
            showCancelButton: true,
            confirmButtonText: 'Eliminar',
            cancelButtonText: 'Cancelar',
          }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                
              Swal.fire('Eliminado!', '', 'success');
              element.parentNode.parentNode.remove();
            } 
          })
    }
    const confirmKardexData = () => {
        Swal.fire({
            title: 'Se agregará el Kardex del periodo, ¿Desea continuar?',
            showCancelButton: true,
            confirmButtonText: 'continuar',
            cancelButtonText: 'Cancelar',
          }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
              document.querySelector("#addKardexPeriod").submit();
            } 
          })
    }

    document.querySelector("#school_period").addEventListener("load click", (e) =>{
        let schoolPeriodInput = document.querySelector("input[name='kardex-school-period']");
        if(schoolPeriodInput){
            schoolPeriodInput.value = document.querySelector("#school_period").value;
        }
    });
    @endif
  </script>
@endsection
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.2.0/dist/select2-bootstrap-5-theme.min.css" />
<!-- Or for RTL support -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.css"/>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.2.0/dist/select2-bootstrap-5-theme.rtl.min.css" />
<!-- Tempus Dominus Styles -->
<link href="https://cdn.jsdelivr.net/gh/Eonasdan/tempus-dominus@master/dist/css/tempus-dominus.css"
      rel="stylesheet" crossorigin="anonymous">

@endsection