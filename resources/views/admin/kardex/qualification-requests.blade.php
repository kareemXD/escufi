@extends('layouts.dashboard')
@section('breadcrumb')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">Dashboard</h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">
              Kardex
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
@endsection
@section('content')
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title mb-0">Solicitudes de aprobación abiertas </h5>
                </div>
                <div class="">
                  @if($errors->any())
                      {!! implode('', $errors->all('<div>:message</div>')) !!}
                  @endif

                </div>
                <div>
                    <table class="table" id="kardex_table">
                        <thead>
                            <tr class="table table-dark text-white">
                                <th>Numero de solicitud</th>
                                <th>Docente</th>
                                <th>Nota</th>
                                <th>Cantidad</th>
                                <th>Estatus</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                           @foreach($kardexConfirmations as $kardexConfirmation)
                           <tr>
                            <td>{{ $kardexConfirmation->request_number }}</td>
                            <td>{{ $kardexConfirmation->professor->full_name }}</td>
                            <td>{{ $kardexConfirmation->description }}</td>
                            <td>{{ $kardexConfirmation->kardexs()->count() }}</td>
                            <td>{{ $kardexConfirmation->qualification_status }}</td>
                            <td><a href="{{ route('admin.kardex.qualification-requests-kardex', $kardexConfirmation) }}" class="btn btn-primary">Detalle</a></td>
                           </tr>
                           @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@include('partials.modals.confirm-kardex-qualification')
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
@endsection
@section('js')
<script src="{{asset('assets/extra-libs/multicheck/datatable-checkbox-init.js')}}"></script>
<script src="{{asset('assets/extra-libs/multicheck/jquery.multicheck.js')}}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/i18n/es.js"></script>

<script type="text/javascript">
    @if(session()->has('success'))
      Swal.fire('{{ session()->get("success") }}');

    @endif


    function showMessageKardex(message){
      Swal.fire(message);
    }

  </script>
  
@endsection
@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.css"/>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" />

@endsection