@extends('layouts.dashboard')
@section('breadcrumb')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">Dashboard</h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">
              Administración de estudiantes
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
@endsection
@section('content')
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @if (session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                      {{ session('success') }}
                      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    @endif
                    @if ($errors->any())
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                      {{ session()->get('errors')->first() }}
                      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    @endif
                    <h5 class="card-title mb-0">Listado de Kardex</h5>
                </div>
                <div class="">
                    <div class="row">
                        <div class="col-md-3 m-2">
                          <label for="search_students">Buscar Estudiante</label>
                          <select id="search_students" class="form-select js-data-ajax"></select>
                        </div>
                        <form method="post" id="formGenerateKardex" action="{{ route('admin.kardex.dinamic-create') }}" class="col-3 m-2">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-md-12">
                                  <label for="search_students">Periodo</label>
                                  <select class="form-select" name="kardex-school-period" >
                                    <option selected value="{{ $currentPeriod->id }}">{{ $currentPeriod->code }}</option>
                                  </select>
                                </div>
                                <div class="d-grid gap-2 mt-2">
                                  <button class="btn btn-secondary">Generar Kardex</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <table class="table" id="kardex_table">
                        <thead>
                            <tr class="table table-dark text-white">
                                <th>Matricula</th>
                                <th>Paterno</th>
                                <th>Materno</th>
                                <th>Nombre</th>
                                <th>Carrera</th>
                                <th>Periodo</th>
                                <th>Estatus</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($kardexs as $kardex)
                                <tr>
                                    <td>{{ $kardex->student->matricula_alumno }}</td>
                                    <td>{{ $kardex->student->paterno }}</td>
                                    <td>{{ $kardex->student->materno }}</td>
                                    <td>{{ $kardex->student->nombre }}</td>
                                    <td>{{ $kardex->student->career->name }}</td>
                                    <td>{{ $kardex->period }}</td>
                                    <td>{{ $kardex->status_kardex }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $kardexs->links()  }}
                </div>
            </div>

        </div>
    </div>

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
@endsection
@section('js')
<script src="{{asset('assets/extra-libs/multicheck/datatable-checkbox-init.js')}}"></script>
<script src="{{asset('assets/extra-libs/multicheck/jquery.multicheck.js')}}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/i18n/es.js"></script>

<script type="text/javascript">
    $(function () {
      $.fn.select2.defaults.set('language', 'es');
      var table = $('#kardex_table').DataTable({
          pageLength: 25,
          dom: 'flptrip',
          language: LNG_ES,
          "bPaginate": false
      });
      $('#search_students').select2({
        ajax: {
          url: '{{ route("admin.kardex.search") }}',
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results:  $.map(data, function (item) {
                    return {
                        text:( item.full_name+ ' '+ item.matricula_alumno),
                        id: item.url_kardex
                    }
                })
            };
          },
          cache: true
          // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
        }
      });
      $('#searchPeriod').select2({
        ajax: {
          url: '{{ route("admin.kardex.search.periods") }}',
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results:  $.map(data, function (item) {
                    return {
                        text:( item.code),
                        id: item.id
                    }
                })
            };
          },
          cache: true
          // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
        }
      });

      $('.js-data-ajax').on('select2:select', function (e) {
        window.location.href = this.value;
      });

    });
  </script>

@endsection
@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.css"/>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" />

@endsection
