@extends('layouts.dashboard')
@section('breadcrumb')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">Dashboard</h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">
              Administración de oferta escolar
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
@endsection
@section('content')
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title mb-0">Oferta escolar</h5>
                    @if(session()->has("success"))
                    <div class="alert alert-primary" role="alert">
                       {{ session()->get('success') }}
                      </div>
                    @endif
                    @if($errors->any())
                      <div class="alert alert-danger" role="alert">
                         {{ session()->get('errors')->first();}}
                        </div>
                    @endif
                    <div class="float-left">
                        <form id="form_filter" class="row needs-validation" action="{{ route('admin.school-offer.list') }}" method="get">
                            <div class="col-md-3">
                                <label class="control-label">Carrera</label>
                                <select required name="career-selected" class="form-select" aria-label="Default select example" id="career_selected">
                                    @foreach($careers as $career)
                                    <option value="{{ $career->id }}">{{ $career->name }}</option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">
                                    Debe seleccionar una carrera
                                </div>
                            </div>
                            <div class="col">
                                <label class="control-label">Semestre</label>
                                <select required placeholder="Seleccione el semestre" name="semester-selected" class="form-select" aria-label="Default select example" id="semester_selected">
                                    <option value="">Seleccione el semestre</option>
                                    @foreach($semesters as $semester)
                                    <option @if($semester->id_semestre == request()->{'semester-selected'}) selected @endif value="{{ $semester->id_semestre }}">{{ $semester->semestre }}</option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">
                                    Debe seleccionar un semestre
                                </div>
                            </div>
                            <div class="col">
                                <label class="control-label">Turno</label>
                                <select required placeholder="Seleccione el turno escolar" name="school-shift-selected" class="form-select" aria-label="Default select example" id="school_shift_selected">
                                    <option value="">Seleccione el turno</option>
                                    @foreach($schoolShifts as $schoolShift)
                                    <option @if($schoolShift->id_turno == request()->{'school-shift-selected'}) selected @endif value="{{ $schoolShift->id_turno }}">{{ $schoolShift->turno }}</option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">
                                    Debe seleccionar un turno escolar
                                </div>
                            </div>
                            <div class="col">
                                <label class="control-label">Grupo</label>
                                <select required placeholder="Seleccione el grupor" name="school-group-selected" class="form-select" aria-label="Default select example" id="school_group_selected">
                                    <option value="">Seleccione el grupo</option>
                                    @foreach($schoolGroups as $schoolGroup)
                                    <option @if($schoolGroup->id == request()->{'school-group-selected'}) selected @endif value="{{ $schoolGroup->id }}">{{ $schoolGroup->code }}</option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">
                                    Debe seleccionar un grupo
                                </div>
                            </div>
                            <div class="col d-flex align-items-end bd-highlight">
                                <button type="submit" id="btn_school_offer" class="btn btn-primary">Filtrar</button>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="mx-3">
                    @if(!empty(request()->{'semester-selected'}))
                    <button class="btn btn-primary" data-bs-target="#modalCreateSchoolOffer" data-bs-toggle="modal"><i class="mdi mdi-plus" ></i></button>
                    @endif
                    <table class="table" id="school-offer-list">
                        <thead>
                            <tr class="table table-dark text-white">
                                <th>Semestre</th>
                                <th>Grupo</th>
                                <th>Carrera</th>
                                <th>Asignatura</th>
                                <th>Profesor</th>
                                <th>Matricula profesor</th>
                                <th>Minimo aprobatorio</th>
                                <th>Creditos</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($schoolOffers) > 0)
                            @foreach($schoolOffers as $schoolOffer)
                            <tr>
                                <td>{{ $schoolOffer->semester->semestre }}</td>
                                <td>{{ $schoolOffer->schoolGroup->code }}</td>
                                <td>{{ ($schoolOffer->career->name) }}</td>
                                <td>{{ ($schoolOffer->signature->asignatura) }}</td>
                                <td>{{ $schoolOffer->professor->full_name }}</td>
                                <td>{{ $schoolOffer->matricula_professor }}</td>
                                <td>{{ number_format($schoolOffer->approving_minimum, 2) }}</td>
                                <td>{{ number_format($schoolOffer->credits, 2) }}</td>
                                <td>
                                    <button data-calendar="{{ json_encode($schoolOffer->schoolOfferCalendars) }}" data-school-offer="{{ json_encode($schoolOffer) }}" data-route="{{ route('admin.school-offer.edit', $schoolOffer) }}" class="btn btn-info text-white" data-bs-target="#modalEditSchoolOffer" data-bs-toggle="modal"><i class="mdi mdi-grease-pencil" ></i></button>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>

</div>
@if(!empty(request()->{'semester-selected'}))
@include('partials.modals.create-school-offer')
@include('partials.modals.edit-school-offer')
@endif
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
@endsection
@section('js')
<script src="{{asset('assets/extra-libs/multicheck/datatable-checkbox-init.js')}}"></script>
<script src="{{asset('assets/extra-libs/multicheck/jquery.multicheck.js')}}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<!-- Popperjs -->
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.2/dist/umd/popper.min.js"
      crossorigin="anonymous"></script>
<!-- Tempus Dominus JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/@eonasdan/tempus-dominus@6.9.4/dist/js/tempus-dominus.min.js"
      crossorigin="anonymous"></script>
<script type="text/javascript">
    const button_filter = document.querySelector("#btn_school_offer");
    const form_filter = document.querySelector("#form_filter");

    form_filter.addEventListener('submit', function (event) {
        let button = form_filter.querySelector('button[type="submit"]');
        blockButton(button);
        if(!form_filter.checkValidity()){
            event.preventDefault()
            event.stopPropagation()
        }
        form_filter.classList.add('was-validated');
      }, false);
      $( '.select2' ).select2( {
        theme: 'bootstrap-5',
        });

        $('.select2-edit').select2({
            dropdownParent: $("#modalEditSchoolOffer"),
            theme: 'bootstrap-5',
        });
        $('.select2-create').select2({
            dropdownParent: $("#modalCreateSchoolOffer"),
            theme: 'bootstrap-5',
        });
        document.querySelectorAll('.timepicker').forEach((element, index) => {
            new tempusDominus.TempusDominus(element, {
                display: {
                  viewMode: 'clock',
                  components: {
                    calendar: false,
                    date:false,
                    decades: false,
                    clock: true,
                    year: false,
                    month: false,
                    date: false,
                    hours: true,
                    minutes: true,
                    seconds: false,
                    useTwentyfourHour: undefined
                  }
                },
                localization:{
                    format: 'HH:mm'
                }
              });
        })


  </script>
@endsection
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.2.0/dist/select2-bootstrap-5-theme.min.css" />
<!-- Or for RTL support -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.css"/>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.2.0/dist/select2-bootstrap-5-theme.rtl.min.css" />
<!-- Tempus Dominus Styles -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@eonasdan/tempus-dominus@6.9.4/dist/css/tempus-dominus.min.css" crossorigin="anonymous">

@endsection
