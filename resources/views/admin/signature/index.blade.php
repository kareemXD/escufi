@extends('layouts.dashboard')
@section('breadcrumb')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">Dashboard</h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">
              Administración de asignaturas
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
@endsection
@section('content')
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title mb-0">Listado de asignaturas</h5>
                </div>
                <div class="">
                    <div class="d-flex">
                        <a href="{{ route('admin.signature.create') }}" class="btn btn-primary mx-1"><i class="mdi mdi-account-plus"></i></a>
                    </div>
                    <table class="table" id="signature_table">
                        <thead>
                            <tr class="table table-dark text-white">
                                <th>Asignatura</th>
                                <th>Creditos</th>
                                <th>Código</th>
                                <th>Estatus</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($signatures as $signature)
                                <tr>
                                    <td>{{ $signature->asignatura }}</td>
                                    <td>{{ number_format($signature->calificacion_aprovatoria, 2) }}</td>
                                    <td>{{ $signature->code }}</td>
                                    <td>
                                        <div class="form-check form-switch">
                                          <input data-change="{{ route('admin.signature.toggle-status', $signature) }}"  class="form-check-input" type="checkbox" id="flexSwitchCheckChecked{{ $signature->id }}" {{  $signature->activa == "A"? 'checked' : ''  }}>
                                          <label class="form-check-label" for="flexSwitchCheckChecked{{ $signature->id }}"></label>
                                        </div>
                                    </td>
                                    <td>
                                      <a class="btn btn-primary" href="{{ route('admin.signature.edit', $signature) }}"><i class="mdi mdi-account-edit text-white"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
         
        </div>
    </div>

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
@endsection
@section('js')
<script src="{{asset('assets/extra-libs/multicheck/datatable-checkbox-init.js')}}"></script>
<script src="{{asset('assets/extra-libs/multicheck/jquery.multicheck.js')}}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.js"></script>
<script type="text/javascript">
  $(function () {        
    var table = $('#signature_table').DataTable({
        pagingType: "full_numbers",
        pageLength: 25,
        dom: 'flptrip',
        processing: true,
        serverSide: true,
        ajax: "{{ route('admin.signature.index') }}",
        columns: [
            {data: 'asignatura', name: 'asignatura'},
            {data: 'calificacion_aprovatoria', name: 'calificacion_aprovatoria'},
            {data: 'clave', name: 'clave'},
            {data: 'activa', name: 'activa'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ],
        language: LNG_ES
    });

    $(document).on("change",  "[data-change]", function(event){
      changeStatusSignature(this.getAttribute("data-change"), this);
    });
      
  });
  const changeStatusSignature = async(route, element) => {
    const location = window.location.hostname;
    const settings = {
        method: 'POST',
        headers: {
            'x-csrf-token': _token,
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
    };
    let status_checked = element.checked;
    try {
        const fetchResponse = await fetch(route, settings);
        const data = await fetchResponse.json();
        if(fetchResponse.status === 422){
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: "no se pudo cambiar el estatus",
          });
          element.checked = !status_checked
          
          return;
        }else if(fetchResponse.status === 200 || fetchResponse.status== 201){
            Swal.fire({
                icon: 'success',
                title: 'Correcto',
                text: 'Estatus cambiado correctamente.',
              });
              element.checked = status_checked;
        }else{
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'No es posible ejecutar la peticion en este momento',
          });
          element.checked = !status_checked
        }
        return data;
    } catch (error) {
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'No es posible ejecutar la peticion en este momento',
      });
      element.checked = !status_checked
    }    
}

  </script>
@endsection
@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.css"/>

@endsection