@extends('layouts.dashboard')
@section('breadcrumb')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">Dashboard</h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ route('admin.signature.index') }}">Asignaturas</a></li>
            <li class="breadcrumb-item active" aria-current="page">
              Registrar asignatura
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
@endsection
@section('content')
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title mb-0">Editar Asignatura</h5>
                    <form  method="post" id="form_signature_create" class="row g-3 needs-validation" novalidate  action="{{ route('admin.signature.store') }}" >
                        @if (session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                          {{ session('success') }}
                          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                        @endif
                        @if ($errors->any())
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                          {{ session()->get('errors')->first() }}
                          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                        @endif
                        {{ csrf_field() }}
                        <hr>
                        <div class="row">
                        <div class="col-md-5">
                            <label for="asignatura" class="control-label">Nombre</label>
                            <input   
                            value="{{ old('asignatura') }}" autocomplete="off" type="text" class="form-control @if($errors->has('asignatura'))
                            is-invalid
                            @endif " name="asignatura" id="asignatura" required="required" minlength="3" maxlength="100">
                            <div class="invalid-feedback">
                            
                            @if($errors->has('asignatura'))
                                {{ $errors->first('asignatura') }}
                            @else
                            La asignatura es obligatoria
                            @endif
                            </div>
                        </div>
                        <div class="col-md-5">
                            <label for="calificacion-aprobatoria" class="control-label">Creditos</label>
                            <input step="0.1" value="{{ old('calificacion-aprobatoria') }}"  autocomplete="off" type="number" class="form-control @if($errors->has('calificacion-aprobatoria'))
                            is-invalid
                            @endif" name="calificacion-aprobatoria" id="calificacion-aprobatoria" required="required" >
                            <div class="invalid-feedback">
                            
                            @if($errors->has('calificacion-aprobatoria'))
                                {{ $errors->first('calificacion-aprobatoria') }}
                            @else
                            Los creditos son requeridos
                            @endif
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label for="clave" class="control-label">clave</label>
                            <input value="{{ old('clave') }}" autocomplete="off" type="text" class="form-control @if($errors->has('clave'))
                            is-invalid
                            @endif" name="clave" id="clave" minlength="3" maxlength="4">
                            <div class="invalid-feedback">
                            
                            @if($errors->has('clave'))
                                {{ $errors->first('clave') }}
                            @else
                            La clave debe contener 4 caracteres
                            @endif
                            </div>
                        </div>
                        <div class="d-grid gap-2 mt-3">
                            <button type="submit" class="btn btn-info text-white" type="button" >Registrar</button>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
         
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
@endsection
@section('js')
<script>
    document.querySelector("#form_signature_create").addEventListener('submit', function (event) {
        let button = this.querySelector('button[type="submit"]');
        blockButton(button);
      
        if (!this.checkValidity()) {
          event.preventDefault()
          event.stopPropagation()
        }
        this.classList.add('was-validated');
      
      }, false);
</script>
@endsection
