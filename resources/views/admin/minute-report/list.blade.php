@extends('layouts.dashboard')
@section('breadcrumb')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">Dashboard</h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">
              Administración de reportes de acta
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
@endsection
@section('content')
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title mb-0">Listado de reportes</h5>
                </div>
                <div class="">
                    <form action="">
                      <div class="row">
                        <div class="col-3">
                          <label for="career_id">Carrera</label>
                          <select required="required" class="form-select" name="career-id" id="career_id">
                            @foreach ($careers as $career)
                              <option  @if(request()->{"career-id"} == $career->id) selected @endif  value="{{ $career->id }}">{{ $career->name }}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="col-3">
                          <label for="period_code">Periodo</label>
                          <select required="required" class="form-select" name="period-code" id="period_code">
                            @foreach ($schoolPeriods as $schoolPeriod)
                              <option @if(request()->{"period-code"} == $schoolPeriod->code) selected @endif value="{{ $schoolPeriod->code }}">{{ $schoolPeriod->code }}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="col-3">
                          <label for="semester_id">Semestre</label>
                          <select required="required" class="form-select" name="semester-id" id="semester_id">
                            @foreach ($semesters as $semester)
                              <option @if(request()->{"semester-id"} == $semester->id_semestre) selected @endif value="{{ $semester->id_semestre }}">{{ $semester->semestre }}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="col-3 pt-4 mt-1">
                          <button type="submit" class="btn btn-primary">Filtrar</button>
                        </div>
                      </div>
                    </form>
                    <table class="table" id="minute_report_table">
                        <thead>
                            <tr class="table table-dark text-white">
                                <th>Materia</th>
                                <th>Docente</th>
                                <th>Periodo</th>
                                <th>Semestre</th>
                                <th>Código</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                          @foreach($listOfMaterias as $listOfMateria)
                          <tr>
                            <td>{{ $listOfMateria->signature->asignatura }}</td>
                            <td>{{ $listOfMateria->professor->full_name }}</td>
                            <td>{{ $listOfMateria->period }}</td>
                            <td>{{ $listOfMateria->semester->semestre }}</td>
                            <td>{{ $listOfMateria->signature->code }}</td>
                            <td>
                              <a href="{{ route('admin.report.print-signature', [
                                'career' => $listOfMateria->career,
                                'signature' => $listOfMateria->signature,
                                'professor' => $listOfMateria->professor,
                                'schoolPeriod' => $listOfMateria->period,
                                'semester' => $listOfMateria->semester
                              ]) }}" class="btn btn-primary">Imprimir</a>
                            </td>
                          </tr>
                          @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
         
        </div>
    </div>

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
@endsection
@section('js')
<script src="{{asset('assets/extra-libs/multicheck/datatable-checkbox-init.js')}}"></script>
<script src="{{asset('assets/extra-libs/multicheck/jquery.multicheck.js')}}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.js"></script>
<script type="text/javascript">
    $(function () {        
      var table = $('#minute_report_table').DataTable({
          pagingType: "full_numbers",
          pageLength: 25,
          dom: 'flptrip',
          language: LNG_ES
      });
      
        
    });
  </script>
@endsection
@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.css"/>

@endsection