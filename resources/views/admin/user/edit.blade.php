@extends('layouts.dashboard')
@section('breadcrumb')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">Dashboard</h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ route('admin.users.index') }}">Usuarios</a></li>
            <li class="breadcrumb-item active" aria-current="page">
              Modificar usuario
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
@endsection
@section('content')
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title mb-0">{{ $user->type_user }}</h5>
                    <h5 class="card-title mb-0">Modificar usuario con identificador {{ $user->matricula }}</h5>
                    <form  method="post" id="form_user_edit" class="row g-3 needs-validation  @if ($errors->any()) was-validated @endif" novalidate  action="{{ route('admin.users.update', $user) }}" >
                        {{  method_field('PUT') }}
                        @if (session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                          {{ session('success') }}
                          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                        @endif
                        {{ csrf_field() }}
                        <div class="col-12">
                          <hr>
                          <div class="row">
                            <div class="col-md-6">
                              <label for="name" class="control-label">Nombre Completo</label>
                              <input type="text" class="form-control" name="name" id="name" required="required" minlength="3" maxlength="100" value="{{ $user->name }}" autocomplete="off" >
                              <div class="invalid-feedback">
                               
                                @if($errors->has('name'))
                                  {{ $errors->first('name') }}
                                @else
                                El nombre es requerido
                                @endif
                              </div>
                            </div>
                            <div class="col-md-6">
                              <label for="email" class="control-label">Correo Electrónico</label>
                              <input  autocomplete="off"  type="email" class="form-control" name="email" id="email" required="required" minlength="3" maxlength="100" value="{{ $user->email }}" >
                              <div class="invalid-feedback">
                                @if($errors->has('email'))
                                {{ $errors->first('email') }}
                              @else
                              El email ingresado no es válido
                              @endif
                              </div>
                            </div>
                            <div class="col-md-6">
                              <label for="password" class="control-label">Contraseña</label>
                              <input  autocomplete="off"  type="password" class="form-control" name="password" id="password" oninput="passwordValidation();"
                              >
                              <div class="invalid-feedback">
                                @if($errors->has('password'))
                                {{ $errors->first('password') }}
                              @else
                                La contraseña ingresada no es permitida
                              @endif
                              </div>
                            </div>
                            <div class="col-md-6">
                              <label for="password_confirmation" class="control-label">Contraseña</label>
                              <input  autocomplete="off"  type="password" same class="form-control" name="password-confirmation" id="password_confirmation" 
                              oninput="passwordConfirmValidation();"
                              >
                              <div class="invalid-feedback"
                              
                              >
                              @if($errors->has('password-confirmation'))
                                {{ $errors->first('password-confirmation') }}
                              @else
                                La contraseña ingresada no coincide
                              @endif
                              </div>
                            </div>
                            <div class="d-grid gap-2 mt-3">
                              <button type="submit" class="btn btn-info text-white" type="button" >Modificar</button>
                            </div>
                          </div>
                        </div>
                    </form>
                </div>
            </div>
         
        </div>
    </div>

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
@endsection
@section('js')
<script>
  const  passwordConfirmValidation = () => {
    password_confirm =  document.querySelector('input[name="password-confirmation"').value == document.querySelector('input[name="password"').value;
    if(!password_confirm){
      document.querySelector('input[name="password-confirmation"').setCustomValidity("La contraseña no coincide.");
    }else{
      document.querySelector('input[name="password-confirmation"').setCustomValidity("");
    }
  };

  const  passwordValidation = () => {
    password_confirm =  document.querySelector('input[name="password"').value == "";
    if(password_confirm){
        document.querySelector('input[name="password-confirmation"').setCustomValidity("");
    }else{
        document.querySelector('input[name="password-confirmation"').setCustomValidity("La contraseña es requerida");
    }
  };

  document.querySelector("#form_user_edit").addEventListener('submit', function (event) {
    let button = this.querySelector('button[type="submit"]');
    blockButton(button);
  
    if (!this.checkValidity()) {
      event.preventDefault()
      event.stopPropagation()
    }
    this.classList.add('was-validated');
  
  }, false);
  

</script>
@endsection
