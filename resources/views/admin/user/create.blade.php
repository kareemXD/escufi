@extends('layouts.dashboard')
@section('breadcrumb')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">Dashboard</h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ route('admin.users.index') }}">Usuarios</a></li>
            <li class="breadcrumb-item active" aria-current="page">
              Registrar usuario
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
@endsection
@section('content')
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title mb-0">Registrar nuevo usuario</h5>
                    <hr>
                    <form   method="post" id="form_user_create" class="row g-3 needs-validation  @if ($errors->any()) was-validated @endif" novalidate  action="{{ route('admin.users.store') }}" >
                        @if (session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                          {{ session('success') }}
                          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                        @endif
                        @if ($errors->any())
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                          {{ session()->get('errors')->first() }}
                          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                        @endif
                        {{ csrf_field() }}
                        <div class="col-md-6">
                            <label for="matricula" class="form-label">Matricula</label>
                            <input  autocomplete="off"  name="matricula" type="text" class="form-control" id="matricula" @if(empty(request()->matricula )) value="{{ request()->matricula }}"  @else value="{{ old('matricula') }}"  @endif required>
                            <div class="invalid-feedback">
                              Necesita una matricula valida
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="type_user" class="form-label">Tipo se usuario</label>
                            <select  autocomplete="off"  placeholder="seleccione el tipo de usuario" class="form-control" name="type-user" id="type_user" required="required">
                                <option @if(request()->{'type-user'} == 'Profesor') selected @endif value="Profesor">Profesor</option>
                                <option @if(request()->{'type-user'} == 'Alumno') selected @endif value="Alumno">Alumno</option>
                                @can('all')
                                    <option @if(request()->{'type-user'} == 'Control Escolar') selected @endif  value="Control Escolar">Control Escolar</option>
                                    <option @if(request()->{'type-user'} == 'Subdireccion') selected @endif  value="Subdireccion">Subdireccion Escolar</option>
                                @endcan
                            </select>
                        </div>
                        <div class="d-grid gap-2">
                          <button data-verify-user-type="{{ route('admin.users.verifyType') }}" id="button_verify" class="btn btn-primary" type="button" >Verificar</button>
                        </div>
                        
                        <div id="user_validation_container" class="d-none col-12">
                          <hr>
                          <div class="row">
                            <div class="col-md-6">
                              <label for="name" class="control-label">Nombre Completo</label>
                              <input  autocomplete="off" type="text" class="form-control" name="name" id="name" required="required" minlength="3" maxlength="100">
                              <div class="invalid-feedback">
                               
                                @if($errors->has('name'))
                                  {{ $errors->first('name') }}
                                @else
                                El nombre es requerido
                                @endif
                              </div>
                            </div>
                            <div class="col-md-6">
                              <label for="email" class="control-label">Correo Electrónico</label>
                              <input autocomplete="off" type="email" class="form-control" name="email" id="email" required="required" minlength="3" maxlength="100">
                              <div class="invalid-feedback">
                                @if($errors->has('email'))
                                {{ $errors->first('email') }}
                              @else
                              El email ingresado no es válido
                              @endif
                              </div>
                            </div>
                            <div class="col-md-6">
                              <label for="password" class="control-label">Contraseña</label>
                              <input type="password" class="form-control" name="password" id="password" required="required" minlength="3" maxlength="100"
                              
                              >
                              <div class="invalid-feedback">
                                @if($errors->has('password'))
                                {{ $errors->first('password') }}
                              @else
                                La contraseña ingresada no es permitida
                              @endif
                              </div>
                            </div>
                            <div class="col-md-6">
                              <label for="password_confirmation" class="control-label">Contraseña</label>
                              <input  autocomplete="off"  type="password" same class="form-control" name="password-confirmation" id="password_confirmation" required="required" minlength="3" maxlength="100" equals
                              oninput="passwordConfirmValidation();"
                              >
                              <div class="invalid-feedback"
                              >
                              @if($errors->has('password-confirmation'))
                                {{ $errors->first('password-confirmation') }}
                              @else
                                La contraseña ingresada no coincide
                              @endif
                              </div>
                            </div>
                            <div class="d-grid gap-2 mt-3">
                              <button type="submit" class="btn btn-info text-white" type="button" >Registrar</button>
                            </div>
                          </div>
                        </div>
                    </form>
                </div>
            </div>
         
        </div>
    </div>

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
@endsection
@section('js')
<script src="{{mix('js/user/create.min.js')}}"></script>
<script>
  const  passwordConfirmValidation = () => {
    data =  document.querySelector('input[name="password-confirmation"').value == document.querySelector('input[name="password"').value;
    if(!data){
      document.querySelector('input[name="password-confirmation"').setCustomValidity("La contraseña no coincide.");
    }else{
      document.querySelector('input[name="password-confirmation"').setCustomValidity("");
    }
  };
</script>
@endsection
