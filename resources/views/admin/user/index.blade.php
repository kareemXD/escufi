@extends('layouts.dashboard')
@section('breadcrumb')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">Dashboard</h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">
              Administración de usuarios del sistema
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
@endsection
@section('content')
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title mb-0">Listado de usuarios activos </h5>
                </div>
                <div class="">
                    <div class="d-flex">
                        <a href="{{ route('admin.users.create') }}" class="btn btn-primary"><i class="mdi mdi-account-plus"></i></a>
                    </div>
                    
                    <table class="table" id="students_list">
                        <thead>
                            <tr class="table table-dark text-white">
                                <th>Matricula</th>
                                <th>Nombre</th>
                                <th>Email</th>
                                <th>Tipo</th>
                                <th>Estatus</th>
                                <th>Acción</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <td>{{$user->matricula}}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->type_user }}</td>
                                    <td>
                                        @can('all')
                                        <div class="form-check form-switch">
                                            <input @can('all') data-user-active="{{ route('admin.user.change_status', $user) }}" @endcan class="form-check-input" type="checkbox" id="flexSwitchCheckChecked{{ $user->id }}" {{  $user->active? 'checked' : ''  }}>
                                            <label class="form-check-label" for="flexSwitchCheckChecked{{ $user->id }}"></label>
                                        </div>
                                        @endcan
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.users.edit', $user) }}" title="editar" class="btn btn-info" ><i class="mdi mdi-account-edit"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
         
        </div>
    </div>

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
@endsection
@section('js')
<script src="{{asset('assets/extra-libs/multicheck/datatable-checkbox-init.js')}}"></script>
<script src="{{asset('assets/extra-libs/multicheck/jquery.multicheck.js')}}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.js"></script>
    <script type="text/javascript">
        
        const changeStatusUser = async(route, element) => {
            const location = window.location.hostname;
            const settings = {
                method: 'POST',
                headers: {
                    'x-csrf-token': _token,
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
            };
 
            let status_checked = element.checked;
            try {
                const fetchResponse = await fetch(route, settings);
                const data = await fetchResponse.json();
                if(fetchResponse.status === 422){
                  Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: data.errors.matricula[0],
                  });
                  element.checked = !status_checked
                  
                  return;
                }else if(fetchResponse.status === 200 || fetchResponse.status== 201){
                    Swal.fire({
                        icon: 'success',
                        title: 'Correcto',
                        text: 'Estatus cambiado correctamente.',
                      });
                      element.checked = status_checked;
                }else{
                  Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'No es posible ejecutar la peticion en este momento',
                  });
                  element.checked = !status_checked
                }
                return data;
            } catch (error) {
              Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'No es posible ejecutar la peticion en este momento',
              });
              element.checked = !status_checked
            }    
        }
        document.querySelectorAll('[data-user-active]').forEach( (el) => {
            el.addEventListener('change', function(){
               let route = el.getAttribute('data-user-active');
               changeStatusUser(route, el);
            });
        });
    </script>
@endsection
@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.css"/>

@endsection