@extends('layouts.dashboard')
@section('breadcrumb')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">Dashboard</h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ route('admin.career.index') }}">Carreras</a></li>
            <li class="breadcrumb-item active" aria-current="page">
              Modificar Carrera
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
@endsection
@section('content')
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title mb-0">Editar Carrera</h5>
                    <form  method="post" id="form_career_edit" class="row g-3 needs-validation" novalidate  action="{{ route('admin.career.update', $career) }}" >
                        {{  method_field('PUT') }}
                        @if (session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                          {{ session('success') }}
                          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                        @endif
                        @if ($errors->any())
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                          {{ session()->get('errors')->first() }}
                          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                        @endif
                        {{ csrf_field() }}
                        <hr>
                        <div class="row">
                        <div class="col-md-12">
                            <label for="career_name" class="control-label">Nombre</label>
                            <input   minlength="5" maxlength="200"
                            value="{{ $career->name }}" autocomplete="off" type="text" class="form-control @if($errors->has('career-name'))
                            is-invalid
                            @endif " name="career-name" id="career_name" required="required" minlength="3" maxlength="100">
                            <div class="invalid-feedback">
                            
                            @if($errors->has('career-name'))
                                {{ $errors->first('career-name') }}
                            @else
                            La carrrera es obligatoria
                            @endif
                            </div>
                        </div>
                        
                        <div class="d-grid gap-2 mt-3">
                            <button type="submit" class="btn btn-info text-white" type="button" >Modificar</button>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
         
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
@endsection
@section('js')
<script>
    document.querySelector("#form_career_edit").addEventListener('submit', function (event) {
        let button = this.querySelector('button[type="submit"]');
        blockButton(button);
      
        if (!this.checkValidity()) {
          event.preventDefault()
          event.stopPropagation()
        }
        this.classList.add('was-validated');
      
      }, false);
</script>
@endsection
