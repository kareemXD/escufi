@extends('layouts.dashboard')
@section('breadcrumb')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">Dashboard</h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">
              Kardex
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
@endsection
@section('content')
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title mb-0">Listado de Kardex con matricula {{ $professor->matricula_professor }}</h5>
                </div>
                <div class="">
                  <form action="">
                    <div class="row">
                      <div class="col-md-3 m-2">
                        <label for="search_school_period">Periodo</label>
                        <select name="search-school-period" id="search_school_period" class="form-select">
                            @foreach($schoolPeriods as $schoolP)
                            <option @if(request()->{"search-school-period"} == $schoolP->code) selected @endif value="{{ $schoolP->code }}">{{$schoolP->code}}</option>
                            @endforeach
                        </select>
                      </div>

                      <div class="col-md-3 m-2">
                        <label for="search_signatures">Materia</label>
                        <select name="search-signature" class="form-select" name="" id="search_signatures">
                          @foreach($signatures as $signature)
                            <option @if(request()->{"search-signature"} == $signature->id_asignatura) selected @endif value="{{ $signature->id_asignatura }}">{{ $signature->asignatura }}</option>
                          @endforeach
                        </select>
                    </div>
                    <div class="col-md-3 m-2">
                      <label for="search_career">Carrera</label>
                      <select name="search-career" id="search_career" class="form-select">
                          @foreach($careers as $career)
                          <option @if(request()->{"search-career"} == $career->id) selected @endif value="{{ $career->id }}">{{$career->name}}</option>
                          @endforeach
                      </select>
                    </div>
                    <div class="col-md-3 m-2">
                      <label for="search_semester">Semestre</label>
                      <select name="search-semester" class="form-select" name="" id="search_semester">
                        @foreach($semesters as $semester)
                          <option @if(request()->{"search-semester"} == $semester->id_semestre) selected @endif value="{{ $semester->id_semestre }}">{{ $semester->semestre }}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col-md-3 m-2">
                      <label for="search_school_shift">Turno escolar</label>
                      <select name="search-school-shift" class="form-select" name="" id="search_school_shift">
                        <option value="">TODOS</option>
                        @foreach($schoolShifts as $schoolShift)
                          <option @if(request()->{"search-school-shift"} == $schoolShift->id_turno) selected @endif value="{{ $schoolShift->id_turno}}">{{ $schoolShift->turno }}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col-md-3 m-2">
                      <label for="search_school_group">Grupo</label>
                      <select name="search-school-group" class="form-select" name="" id="search_school_group">
                        <option value="">TODOS</option>
                        @foreach($schoolGroups as $schoolGroup)
                          <option @if(request()->{"search-school-group"} == $schoolGroup->id) selected @endif value="{{ $schoolGroup->id}}">{{ $schoolGroup->name }}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col-md-3 d-flex align-items-end bd-highligh mb-2">
                      <button type="submit" class="btn btn-primary" >Buscar</button>
                    </div>
                  </form>
                </div>
                    <table class="table" id="kardex_table">
                        <thead>
                            <tr class="table table-dark text-white">
                                <th>Alumno</th>
                                <th>Matricula</th>
                                <th>Carrera</th>
                                <th>Semestre</th>
                                <th>Minimo aprobatorio</th>
                                <th>Porcentaje de asistencia</th>
                                <th>Calificación</th>
                                <th>Estatus</th>
                                <th>Aprobado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($kardexs as $kardex)
                                <tr>
                                    <td>{{ $kardex->student->full_name }}</td>
                                    <td>{{ $kardex->student->matricula_alumno }}</td>
                                    <td>{{ $kardex->student->career->name }}</td>
                                    <td>{{ $kardex->semester->semestre }}</td>
                                    <td>{{ number_format($kardex->approved_minimum, 2) }}</td>
                                    <td>{{ $kardex->assist_percent. "%" }}</td>
                                    <td class="qualification">{{ number_format($kardex->prequalification,2) }}</td>
                                    <td>{{ $kardex->status_kardex }}</td>
                                    <td>{{ $kardex->approved_status }}</td>
                                    <td>
                                      @if($kardex->kardex_qualification_status == null ||  $kardex->kardex_qualification_status=="rechazado")
                                      <button data-bs-toggle="modal" data-bs-target="#ModalModifyQualification" data-save="{{ route('kardex.professor.update', $kardex) }}" type="button" class="btn btn-info" ><i class="fa fa-pen text-white"></i></button>
                                      @elseif($kardex->kardex_qualification_status == "revision")
                                      <button onclick="showMessageKardex('enviado a revision')" type="button" class="btn btn-info" ><i class="fa fa-envelope-circle-check text-white"></i></button>
                                      @else
                                      <button onclick="showMessageKardex('calificación confirmada')" type="button" class="btn btn-info" ><i class="fa fa-check text-white"></i></button>
                                      @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @if(count($kardexs) === 0)
                <p class="text-center text-muted">
                  No hay kardex para este periodo
                 </p>
                @endif
                @if(!empty(request()->get("search-school-group")) 
                      && !empty(request()->get("search-career")) 
                      && !empty(request()->get("search-school-period"))
                      && !empty(request()->get("search-semester"))
                      && !empty(request()->get("search-school-shift"))
                      && count($kardexs) > 0
                )
                <div class="d-grid gap-2">
                  <a href="{{ route('report.professor.print-signature', [
                    'career' => request()->{'search-career'},
                    'signature' => request()->{'search-signature'},
                    'schoolPeriod' => request()->{'search-school-period'},
                    'semester' => request()->{'search-semester'},
                    'schoolShift' => request()->{'search-school-shift'},
                    'schoolGroup' => request()->{'search-school-group'},
                    'professor' => auth()->user()->professor
                  ]) }}" id="minute_report" class="btn btn-primary" type="button">Reporte de acta</a>
                  <button data-route-confirm="{{ route('kardex.professor.send-confirm', [
                    'career' => request()->{'search-career'},
                    'signature' => request()->{'search-signature'},
                    'schoolPeriod' => request()->{'search-school-period'},
                    'semester' => request()->{'search-semester'},
                    'schoolShift' => request()->{'search-school-shift'},
                    'schoolGroup' => request()->{'search-school-group'}
                  ]) }}" id="qualifications_confirm" class="btn btn-primary" type="button">Mandar calificaciones a revision</button>
                </div>
                <p class="text-center text-muted">
                 Las calificaciones en el reporte de acta se verán reflejadas hasta que se confirmen de forma digital por control escolar.
                </p>
                @endif
            </div>
         
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="ModalModifyQualification" tabindex="-1" aria-labelledby="ModalModifyQualificationLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="ModalModifyQualificationLabel">Modificar calificación</h5>
            <h6 class="modal-subtitle" id="StudentDataLabel"></h6>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label for="modal_prequalification" class="control-label">Calificación</label>
              <input class="form-control" type="number" step="0.01" placeholder="Ingrese calificación de alumno"  id="modal_prequalification" name="modal-prequalification">
              <div class="invalid-feedback">
                
              </div>
            </div>
            <div class="form-group">
              <label for="modal_assist_percent" class="control-label">Porcentaje de asistencia</label>
              <div class="input-group">
                <input id="modal_assist_percent" name="modal-assist-percent" type="number" step="0.01" class="form-control" aria-label="Porcentaje">
                <span class="input-group-text">%</span>
              </div>
              <div class="invalid-feedback">
                
              </div>
            </div>
            <div class="form-group">
              <label for="modal_approved_status">¿Aprobado?</label>
              <select name="modal-approved-status" id="modal_approved_status" class="form-select">
                <option selected="selected" value="null">Seleccione estatus aprobado o no aprobado</option>
                @foreach(App\Models\Kardex::APPROVED_STATUS as $approved_status)
                  <option value="{{ $approved_status }}">{{ strtoupper($approved_status) }}</option>
                @endforeach
              </select>
              <div class="invalid-feedback">
                
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
            <button data-route="" id="btn_save_changes" type="button" class="btn btn-primary">Guardar cambios</button>
          </div>
        </div>
      </div>
    </div>
    <!--End Modal -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
@endsection
@section('js')
<script src="{{asset('assets/extra-libs/multicheck/datatable-checkbox-init.js')}}"></script>
<script src="{{asset('assets/extra-libs/multicheck/jquery.multicheck.js')}}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/i18n/es.js"></script>

<script type="text/javascript">
  var modalQualifications = document.getElementById('ModalModifyQualification');
  modalQualifications.addEventListener('show.bs.modal', async function (event) {
    window.this_cus = this;
    let buttonTarget = event.relatedTarget;
    let data =  await getData(buttonTarget.dataset.save);
    this.querySelector("#btn_save_changes").setAttribute("data-route", buttonTarget.dataset.save);
    window.buttonModal = buttonTarget;
    this.querySelector("#modal_prequalification").value = data.prequalification;
    this.querySelector("#modal_assist_percent").value = data.assist_percent;
    this.querySelector("#modal_approved_status").value = data.approved_status;
  })

  document.querySelector("#qualifications_confirm").addEventListener("click", function(event){
    Swal.fire({
        title: '¿Mandar a revision?',
        showDenyButton: false,
        showCancelButton: true,
        confirmButtonText: 'Enviar',
        cancelButtonText: `Cancelar`,
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
          sendConfirmation(this.getAttribute('data-route-confirm'), this);
        }
      });
  });

  document.querySelector("#btn_save_changes").addEventListener("click", function(event){

    let prequalification = document.querySelector("#modal_prequalification").value;
    let assist_percent = document.querySelector("#modal_assist_percent").value;
    let approved_status = document.querySelector("#modal_approved_status").value;
    let route = this.getAttribute("data-route");
    let data = {
      prequalification: prequalification,
      assist_percent:assist_percent,
      approved_status: approved_status
    };
    if(validateInputsModal()){
      saveDataConfirm(route, data,this ,window.buttonModal);
    }

  });

  async function saveDataConfirm(route, dataRequest, button, buttonElement){
    let old_html = button.innerHTML;
    button.setAttribute("disabled", "disabled");
    button.innerHTML = `<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
    Guardando...`;
    const settings = {
          method: 'PUT',
          headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              'X-CSRF-TOKEN': document.querySelector("meta[name='csrf-token']").getAttribute("content")
          },
          body: JSON.stringify(dataRequest)
      };
      try{
      const response = await fetch(route, settings);
      const data = await response.json();
        if(response.ok){
          if(data.data.assist_percent != null){
            buttonElement.parentNode.parentNode.querySelectorAll("td")[5].innerHTML = data.data.assist_percent+"%";
          }
          if(data.data.prequalification > 0){
            buttonElement.parentNode.parentNode.querySelectorAll("td")[6].innerHTML = parseFloat(data.data.prequalification).toFixed(2);
          }
          buttonElement.parentNode.parentNode.querySelectorAll("td")[8].innerHTML = data.data.approved_status;

          Swal.fire({
            title: "Correcto",
            icon: "success",
            text: "se actualizó el registro correctamente"
          });
        }else{
          Swal.fire({
            title: "Ups",
            icon: "error",
            text: "Algo falló al actualizar, vuelva a intentar"
          });
        }

        button.innerHTML = old_html;
        button.removeAttribute("disabled");

      }catch(error){
        Swal.fire({
          title: "Ups!",
          icon: "error",
          text: "Algo no funcionó al actualizar."
        });
      }
      
  }

  async function getData(route){
    const settings = {
          method: 'GET',
          headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json'
          },
      };

      const response = await fetch(route, settings);
      const data = await response.json();
      console.log(response, data);
      return data;
  }

  async function sendConfirmation(route, button){
    let old_html = button.innerHTML;
    button.setAttribute("disabled", "disabled");
    button.innerHTML = `<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
    Enviando...`;
    const settings = {
          method: 'POST',
          headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              'X-CSRF-TOKEN': document.querySelector("meta[name='csrf-token']").getAttribute("content")
          }
      };
      try{
        const response = await fetch(route, settings);
        const results = await response.json();

        if(response.ok){
          if(results.quantity > 0){
            Swal.fire({
              title: "Correcto",
              icon: "success",
              text: `Se mandaron a confirmar ${results.quantity} calificaciones a revisión`
            });
          }else{
            Swal.fire({
              title: "Ya no hay calificaciones para confirmar",
              icon: "success",
            });
          }
        }else{
          Swal.fire({
            title: "Ups!",
            icon: "error",
            text: "Algo no funcionó al mandar datos."
          });
        }
        button.innerHTML = old_html;
        button.removeAttribute("disabled");
        setTimeout(()=>{
          window.location.reload();
        }, 2000);

      }catch(error){
        Swal.fire({
          title: "Ups!",
          icon: "error",
          text: "Algo no funcionó al mandar datos."
        });
        setTimeout(()=>{
          //window.location.reload();
        }, 3000);
      }
  }
  function showMessageKardex(message){
    Swal.fire(message);
  }

  function isNumeric(str) {
    if (typeof str != "string") return false // we only process strings!  
    return !isNaN(str) && // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
           !isNaN(parseFloat(str)) // ...and ensure strings of whitespace fail
  }

  function validateInputsModal(){
    const prequalificationInput = document.querySelector("#modal_prequalification");
    const assistPercentInput = document.querySelector("#modal_assist_percent");
    const approvedStatusInput = document.querySelector("#modal_approved_status");
    let countErrors = 0;
    if(!(prequalificationInput.value.trim() != "")){
      prequalificationInput.classList.add("is-invalid");
      prequalificationInput.parentNode.querySelector('.invalid-feedback').innerHTML = 'Este dato no debe estar vacio';
      countErrors++;
    }else if(!isNumeric(prequalificationInput.value)){
      prequalificationInput.classList.add("is-invalid");
      prequalificationInput.parentNode.querySelector('.invalid-feedback').innerHTML = 'Este dato debe ser numérico';
      countErrors++;
    }else{
      prequalificationInput.classList.remove("is-invalid");
      prequalificationInput.classList.add("is-valid");
      prequalificationInput.parentNode.querySelector('.invalid-feedback').innerHTML = '';
    }

    if(!(assistPercentInput.value.trim() != "")){
      assistPercentInput.classList.add("is-invalid");
      assistPercentInput.parentNode.classList.add("is-invalid");
      assistPercentInput.parentNode.parentNode.querySelector('.invalid-feedback').innerHTML = 'Este dato no debe estar vacio o ser inválido.';
      countErrors++;
    }else if(!isNumeric(assistPercentInput.value)){
      alert("entra")
      assistPercentInput.classList.add("is-invalid");
      assistPercentInput.parentNode.classList.add("is-invalid");
      assistPercentInput.parentNode.parentNode.querySelector('.invalid-feedback').innerHTML = 'Este dato debe ser numérico';
      countErrors++;
    }else{
      assistPercentInput.classList.remove("is-invalid");
      assistPercentInput.classList.add("is-valid");
      assistPercentInput.parentNode.classList.remove("is-invalid");
      assistPercentInput.parentNode.classList.add("is-valid");
      assistPercentInput.parentNode.parentNode.querySelector('.invalid-feedback').innerHTML = '';
    }

    if(!(approvedStatusInput.value.trim() != "")){
      approvedStatusInput.classList.add("is-invalid");
      approvedStatusInput.parentNode.querySelector('.invalid-feedback').innerHTML = 'Este dato no debe estar vacio';
      countErrors++;
    }else if(!(approvedStatusInput.value == "A" || approvedStatusInput.value=="NA")){
      approvedStatusInput.classList.add("is-invalid");
      approvedStatusInput.parentNode.querySelector('.invalid-feedback').innerHTML = 'Valor no permitido';
      countErrors++;
    }else{
      approvedStatusInput.classList.remove("is-invalid");
      approvedStatusInput.classList.add("is-valid");
      approvedStatusInput.parentNode.querySelector('.invalid-feedback').innerHTML = '';
    }
    
    return (countErrors === 0);
  }

</script>
  
@endsection
@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.css"/>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" />

@endsection