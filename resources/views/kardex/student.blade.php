@extends('layouts.dashboard')
@section('breadcrumb')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">Dashboard</h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">
              Kardex
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
@endsection
@section('content')
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title mb-0">Listado de Kardex con matricula {{ $student->matricula_alumno }}</h5>
                </div>
                <div class="">
                    <div class="col-md-3 m-2">
                      <label for="search_students">Periodo</label>
                      <select id="search_students" class="form-select">
                          @foreach($schoolPeriods as $schoolP)
                          <option value="{{ route('kardex', $schoolP->code) }}">{{$schoolP->code}}</option>
                          @endforeach
                      </select>
                  </div>
                    <table class="table" id="kardex_table">
                        <thead>
                            <tr class="table table-dark text-white">
                                <th>Asignatura</th>
                                <th>Docente</th>
                                <th>Carrera</th>
                                <th>Semestre</th>
                                <th>Minimo aprobatorio</th>
                                <th>Creditos</th>
                                <th>Calificación</th>
                                <th>Estatus</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($schoolPeriod->kardexs as $kardex)
                                <tr>

                                    <td>{{ $kardex->signature->asignatura }}</td>
                                    <td>{{ $kardex->professor->full_name }}</td>
                                    <td>{{ $kardex->student->career->name }}</td>
                                    <td>{{ $kardex->semester->semestre }}</td>
                                    <td>{{ number_format($kardex->approved_minimum, 2) }}</td>
                                    <td>{{ $kardex->credits }}</td>
                                    @if($kardex->prequalification != null && $kardex->prequalification > 0)
                                    <td>{{ number_format($kardex->prequalification,2) }}</td>
                                    @else
                                    <td>0.0</td>
                                    @endif
                                    <td>{{ $kardex->status_kardex }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
@endsection
@section('js')
<script src="{{asset('assets/extra-libs/multicheck/datatable-checkbox-init.js')}}"></script>
<script src="{{asset('assets/extra-libs/multicheck/jquery.multicheck.js')}}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/i18n/es.js"></script>

<script type="text/javascript">
    $(function () {

        $('#kardexStudent').on('select2:select', function (e) {
            window.location.href = this.value;
          });

    });
  </script>

@endsection
@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.11.5/datatables.min.css"/>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" />

@endsection
