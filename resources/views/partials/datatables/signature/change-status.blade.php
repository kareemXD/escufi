<div class="form-check form-switch">
<input data-change="{{ route('admin.signature.toggle-status', $signature) }}"  class="form-check-input" type="checkbox" id="flexSwitchCheckChecked{{ $signature->id }}" {{  $signature->activa == "A"? 'checked' : ''  }}>
<label class="form-check-label" for="flexSwitchCheckChecked{{ $signature->id }}"></label>
</div>