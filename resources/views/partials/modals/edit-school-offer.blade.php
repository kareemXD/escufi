<!-- Modal -->
<div class="modal fade" id="modalEditSchoolOffer" aria-labelledby="modalEditSchoolOfferLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalEditSchoolOfferLabel">Modificar Oferta escolar</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
            <form method="post" id="form_edit_school_offer" action="" class="needs-validation row" novalidate>
                {{ csrf_field() }}
                @method('put')
                <input type="hidden" name="edit-career-id" value="{{ request()->{'career-selected'} }}">
                <input type="hidden" name="edit-semester-id" value="{{  request()->{'semester-selected'} }}">
                <input type="hidden" name="edit-school-shift-id" value="{{  request()->{'school-shift-selected'} }}">
                <input type="hidden" name="edit-school-group-id" value="{{  request()->{'school-group-selected'} }}">
                <div class="col-md-12">
                    <label for="edit_professor">Profesor</label>
                    <select class="form-select select2-edit" name="edit-professor" id="edit_professor">
                        @foreach($professors as $professor)
                        <option value="{{ $professor->matricula_professor }}">{{ $professor->full_name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-12">
                    <label for="edit_signature">Asignatura</label>
                    <select class="form-select select2-edit" name="edit-signature" id="edit_signature">
                        @foreach($signatures as $signature)
                        <option value="{{ $signature->id_asignatura }}">{{ $signature->asignatura }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="edit_approving_minimum">Minimo aprobatorio</label>
                    <input name="edit-approving-minimum" id="edit_approving_minimum" type="number" step="0.01" class="form-control" required="required">
                </div>
                <div class="col-md-6">
                    <label for="edit_credits">Creditos</label>
                    <input name="edit-credits" id="edit_credits" type="number" step="0.01" class="form-control" required="required">
                </div>
                <div class="w-100"><hr></div>
                
                <div class="col-md-3">
                    <label for="edit_checkin">Hora entrada</label>
                    <div class='input-group' data-td-target-input='nearest' data-td-target-toggle='nearest' id="edit_checkin_parent" >
                        <input autocomplete="off" id='edit_checkin' type='text'  class='form-control timepicker' data-td-target='#edit_checkin_parent' />
                        <span class='input-group-text' data-td-target='#edit_checkin' data-td-toggle='datetimepicker' > <span class='fa-solid fa-clock'></span> </span>
                    </div>
                </div>
                <div class="col-md-3">
                    <label for="edit_checkout">Hora salida</label>
                    <div class='input-group' data-td-target-input='nearest' data-td-target-toggle='nearest' id="edit_checkout_parent" >
                        <input autocomplete="off" id='edit_checkout' type='text'  class='form-control timepicker' data-td-target='#edit_checkout_parent' />
                        <span class='input-group-text' data-td-target='#edit_checkout' data-td-toggle='datetimepicker' > <span class='fa-solid fa-clock'></span> </span>
                    </div>
                </div>
                <div class="col-md-5">
                    <label for="edit_days_of_week">Días de la semana</label>
                   
                    <select autocomplete="off" class="form-select select2"  id="edit_days_of_week"  multiple="multiple">
                        @inject('schoolOfferConstant', App\Models\SchoolOfferCalendar::class)
                        @foreach ($schoolOfferConstant::DAYS_OF_WEEK as $day)
                        <option value="{{ $day }}">{{ $day }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-1 mt-2">
                    <label for=""></label>
                    <button type="button" id="edit_add_hours" class="btn btn-primary">Agregar</button>
                </div>
                <div class="row" id="container_add_hours_edit">

                </div>
                <div class="row">
                    <div class="col-md-6 offset-2">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Dia</th>
                                    <th>Hora entrada</th>
                                    <th>Hora salida</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody id="edit_add_data_table">
                                
                            </tbody>
                        </table>
                        
                    </div>
                <div class="d-grid gap-2 mt-3">
                    <button type="submit" class="btn btn-info text-white" type="submit" >Modificar</button>
                </div>
                <div class="d-grid gap-2 mt-3">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                </div>
                </div>
            </form>
        </div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<script>
    const buttonEditAdd = document.querySelector("#edit_add_hours");
    buttonEditAdd.addEventListener("click", (event) => {
        event.preventDefault();
        blockButton(buttonEditAdd);
        verifyEditInputHours();
    });
    const verifyEditInputHours = () => {
        let checkin = document.querySelector("#edit_checkin");
        let checkout = document.querySelector("#edit_checkout");
        let days_of_week = document.querySelector("#edit_days_of_week");
        let is_valid = true;
        //validate checkin
        if(checkin.value == ''){
            is_valid = false;
            if(checkin.classList.contains("is-valid")){
                checkin.classList.remove("is-valid");
            }
            if(!checkin.classList.contains("is-invalid")){
                checkin.classList.add("is-invalid");
            }
        }else{
            if(!checkin.classList.contains("is-valid")){
                checkin.classList.add("is-invalid");
            }
            if(checkin.classList.contains("is-invalid")){
                checkin.classList.remove("is-invalid");
            }
        }
        //validate checkout
        if(checkout.value == ''){
            is_valid = false;
            if(checkout.classList.contains("is-valid")){
                checkout.classList.remove("is-valid");
            }
            if(!checkout.classList.contains("is-invalid")){
                checkout.classList.add("is-invalid");
            }
        }else{
            if(!checkout.classList.contains("is-valid")){
                checkout.classList.add("is-invalid");
            }
            if(checkout.classList.contains("is-invalid")){
                checkout.classList.remove("is-invalid");
            }
        }
        //validate days of week
        if(days_of_week.value == ''){
            is_valid = false;
            if(days_of_week.classList.contains("is-valid")){
                days_of_week.classList.remove("is-valid");
            }
            if(!days_of_week.classList.contains("is-invalid")){
                days_of_week.classList.add("is-invalid");
            }
        }else{
            if(!days_of_week.classList.contains("is-valid")){
                days_of_week.classList.add("is-invalid");
            }
            if(days_of_week.classList.contains("is-invalid")){
                days_of_week.classList.remove("is-invalid");
            }
        }
        if(is_valid){

            let data = {
                checkin:checkin,
                checkout:checkout,
                days_of_week:Array.from(days_of_week.selectedOptions).map(({ value }) => value),
                select_days: days_of_week
            };
            addEditCalendar(data);
            return true;
        }else{
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'debe llenar los campos marcados',
              });
              return false;
        }
    }

    const addEditCalendar = (data) => {
        let container = document.querySelector("#container_add_hours_edit");
        let table_container = document.querySelector("#edit_add_data_table");
        let htmlData = table_container.innerHTML;
        let newHtml = ``;
        data.days_of_week.forEach((day) => {
            newHtml += `<tr>
                <td>${day}
                    <input value="${day}" type="hidden" name="edit-calendar-day-of-week[]" required>
                </td>
                <td>${data.checkin.value}
                    <input value="${data.checkin.value}" type="hidden" name="edit-calendar-checkin[]" required>
                </td>
                <td>${data.checkout.value}
                    <input value="${data.checkout.value}" type="hidden" name="edit-calendar-checkout[]" required>
                </td>
                <td><button onclick="deleteParentButton(this)" class="btn btn-danger" data-remove="" ><i class="fa fa-trash"></i></button></td>
            </tr>`;
        });
        table_container.innerHTML = htmlData + newHtml;
        data.checkin.value = '';
        data.checkout.value = '';
        data.select_days.value = '';
        data.select_days.dispatchEvent(new Event('change'));
    };

    const formedit = document.querySelector("#form_edit_school_offer");
    formedit.addEventListener('submit', function (event) {
        if (!formedit.checkValidity()) {
          event.preventDefault()
          event.stopPropagation()
        }

        formedit.classList.add('was-validated');
      }, false);

      const modalEdit = document.getElementById('modalEditSchoolOffer');
      modalEdit.addEventListener('shown.bs.modal',  (e) => {
            try{

                let schoolOffer = JSON.parse(e.relatedTarget.getAttribute('data-school-offer'));
                formedit['edit-professor'].value = schoolOffer.matricula_professor;
                formedit['edit-professor'].dispatchEvent(new Event('change'));
                formedit['edit-signature'].value = schoolOffer.signature_id;
                formedit['edit-signature'].dispatchEvent(new Event('change'));
                formedit['edit-approving-minimum'].value = parseFloat(schoolOffer.approving_minimum);
                formedit['edit-credits'].value = schoolOffer.credits;
                formedit.setAttribute('action', e.relatedTarget.getAttribute('data-route'));

                console.log(formedit, schoolOffer);

                let table_container = document.querySelector("#edit_add_data_table");
                table_container.innerHTML = ``;
                let newHtml = ``;
                schoolOffer.school_offer_calendars.forEach((school_offer_calendar) => {
                    newHtml += `<tr>
                        <td>${school_offer_calendar.day_of_week}
                            <input value="${school_offer_calendar.day_of_week}" type="hidden" name="edit-calendar-day-of-week[]" required>
                        </td>
                        <td>${moment(school_offer_calendar.checkin, 'HH:mm').format('HH:mm')}
                            <input value="${moment(school_offer_calendar.checkin, 'HH:mm').format('HH:mm')}" type="hidden" name="edit-calendar-checkin[]" required>
                        </td>
                        <td>${moment(school_offer_calendar.checkout, 'HH:mm').format('HH:mm')}
                            <input value="${moment(school_offer_calendar.checkout, 'HH:mm').format('HH:mm')}" type="hidden" name="edit-calendar-checkout[]" required>
                        </td>
                        <td><button onclick="deleteParentButton(this)" class="btn btn-danger" data-remove="" ><i class="fa fa-trash"></i></button></td>
                    </tr>`;
                });
                table_container.innerHTML = newHtml;

            }catch(err){
                Swal.fire({
                    icon: "error",
                    title: "hubo un error"
                });
                console.log(err);
                return;
            }
        });

</script>