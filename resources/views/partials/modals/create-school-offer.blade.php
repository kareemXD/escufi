<!-- Modal -->
<div class="modal fade" id="modalCreateSchoolOffer" aria-labelledby="modalCreateSchoolOfferLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalCreateSchoolOfferLabel">Registrar Oferta escolar</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
            <form method="post" id="form_create_school_offer" action="{{ route('admin.school-offer.store') }}" class="needs-validation row" novalidate>
                {{ csrf_field() }}
                <input type="hidden" name="create-career-id" value="{{ request()->{'career-selected'} }}">
                <input type="hidden" name="create-semester-id" value="{{  request()->{'semester-selected'} }}">
                <input type="hidden" name="create-school-shift-id" value="{{  request()->{'school-shift-selected'} }}">
                <input type="hidden" name="create-school-group-id" value="{{  request()->{'school-group-selected'} }}">
                <div class="col-md-12">
                    <label for="create_professor">Profesor</label>
                    <select class="form-select select2-create" name="create-professor" id="create_professor">
                        @foreach($professors as $professor)
                        <option value="{{ $professor->matricula_professor }}">{{ $professor->full_name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-12">
                    <label for="create_signature">Asignatura</label>
                    <select class="form-select select2-create" name="create-signature" id="create_signature">
                        @foreach($signatures as $signature)
                        <option value="{{ $signature->id_asignatura }}">{{ $signature->asignatura }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="create_approving_minimum">Minimo aprobatorio</label>
                    <input name="create-approving-minimum" id="create_approving_minimum" type="number" step="0.01" class="form-control" required="required">
                </div>
                <div class="col-md-6">
                    <label for="create_credits">Creditos</label>
                    <input name="create-credits" id="create_credits" type="number" step="0.01" class="form-control" required="required">
                </div>
                <div class="w-100"><hr></div>
                
                <div class="col-md-3">
                    <label for="create_checkin">Hora entrada</label>
                    <div class='input-group' data-td-target-input='nearest' data-td-target-toggle='nearest' id="create_checkin_parent" >
                        <input autocomplete="off" id='create_checkin' type='text'  class='form-control timepicker' data-td-target='#create_checkin_parent' />
                        <span class='input-group-text' data-td-target='#create_checkin' data-td-toggle='datetimepicker' > <span class='fa-solid fa-clock'></span> </span>
                    </div>
                </div>
                <div class="col-md-3">
                    <label for="create_checkout">Hora salida</label>
                    <div class='input-group' data-td-target-input='nearest' data-td-target-toggle='nearest' id="create_checkout_parent" >
                        <input autocomplete="off" id='create_checkout' type='text'  class='form-control timepicker' data-td-target='#create_checkout_parent' />
                        <span class='input-group-text' data-td-target='#create_checkout' data-td-toggle='datetimepicker' > <span class='fa-solid fa-clock'></span> </span>
                    </div>
                </div>
                <div class="col-md-5">
                    <label for="create_days_of_week">Días de la semana</label>
                   
                    <select autocomplete="off" class="form-select select2"  id="create_days_of_week"  multiple="multiple">
                        @inject('schoolOfferConstant', App\Models\SchoolOfferCalendar::class)
                        @foreach ($schoolOfferConstant::DAYS_OF_WEEK as $day)
                        <option value="{{ $day }}">{{ $day }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-1 mt-2">
                    <label for=""></label>
                    <button type="button" id="create_add_hours" class="btn btn-primary">Agregar</button>
                </div>
                <div class="row" id="container_add_hours">

                </div>
                <div class="row">
                    <div class="col-md-6 offset-2">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Dia</th>
                                    <th>Hora entrada</th>
                                    <th>Hora salida</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody id="create_add_data_table">
                                
                            </tbody>
                        </table>
                        
                    </div>
                <div class="d-grid gap-2 mt-3">
                    <button type="submit" class="btn btn-info text-white" type="submit" >Registrar</button>
                </div>
                <div class="d-grid gap-2 mt-3">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                </div>
                </div>
            </form>
        </div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<script>
    const buttonAdd = document.querySelector("#create_add_hours");
    buttonAdd.addEventListener("click", (event) => {
        event.preventDefault();
        blockButton(buttonAdd);
        verifyInputHours();
    });
    const verifyInputHours = () => {
        let checkin = document.querySelector("#create_checkin");
        let checkout = document.querySelector("#create_checkout");
        let days_of_week = document.querySelector("#create_days_of_week");
        let is_valid = true;
        //validate checkin
        if(checkin.value == ''){
            is_valid = false;
            if(checkin.classList.contains("is-valid")){
                checkin.classList.remove("is-valid");
            }
            if(!checkin.classList.contains("is-invalid")){
                checkin.classList.add("is-invalid");
            }
        }else{
            if(!checkin.classList.contains("is-valid")){
                checkin.classList.add("is-invalid");
            }
            if(checkin.classList.contains("is-invalid")){
                checkin.classList.remove("is-invalid");
            }
        }
        //validate checkout
        if(checkout.value == ''){
            is_valid = false;
            if(checkout.classList.contains("is-valid")){
                checkout.classList.remove("is-valid");
            }
            if(!checkout.classList.contains("is-invalid")){
                checkout.classList.add("is-invalid");
            }
        }else{
            if(!checkout.classList.contains("is-valid")){
                checkout.classList.add("is-invalid");
            }
            if(checkout.classList.contains("is-invalid")){
                checkout.classList.remove("is-invalid");
            }
        }
        //validate days of week
        if(days_of_week.value == ''){
            is_valid = false;
            if(days_of_week.classList.contains("is-valid")){
                days_of_week.classList.remove("is-valid");
            }
            if(!days_of_week.classList.contains("is-invalid")){
                days_of_week.classList.add("is-invalid");
            }
        }else{
            if(!days_of_week.classList.contains("is-valid")){
                days_of_week.classList.add("is-invalid");
            }
            if(days_of_week.classList.contains("is-invalid")){
                days_of_week.classList.remove("is-invalid");
            }
        }
        if(is_valid){

            let data = {
                checkin:checkin,
                checkout:checkout,
                days_of_week:Array.from(days_of_week.selectedOptions).map(({ value }) => value),
                select_days: days_of_week
            };
            addCalendar(data);
            return true;
        }else{
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'debe llenar los campos marcados',
              });
              return false;
        }
    }

    const addCalendar = (data) => {
        let container = document.querySelector("#container_add_hours");
        let table_container = document.querySelector("#create_add_data_table");
        let htmlData = table_container.innerHTML;
        let newHtml = ``;
        data.days_of_week.forEach((day) => {
            newHtml += `<tr>
                <td>${day}
                    <input value="${day}" type="hidden" name="create-calendar-day-of-week[]" required>
                </td>
                <td>${data.checkin.value}
                    <input value="${data.checkin.value}" type="hidden" name="create-calendar-checkin[]" required>
                </td>
                <td>${data.checkout.value}
                    <input value="${data.checkout.value}" type="hidden" name="create-calendar-checkout[]" required>
                </td>
                <td><button onclick="deleteParentButton(this)" class="btn btn-danger" data-remove="" ><i class="fa fa-trash"></i></button></td>
            </tr>`;
        });
        table_container.innerHTML = htmlData + newHtml;
        data.checkin.value = '';
        data.checkout.value = '';
        data.select_days.value = '';
        data.select_days.dispatchEvent(new Event('change')); 
    };

    const formCreate = document.querySelector("#form_create_school_offer");
    formCreate.addEventListener('submit', function (event) {
        if (!formCreate.checkValidity()) {
          event.preventDefault()
          event.stopPropagation()
        }

        formCreate.classList.add('was-validated');
      }, false);


</script>