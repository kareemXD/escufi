<!-- Modal -->
<div class="modal fade" id="modalConfirm" aria-labelledby="modalConfirmLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalConfirmLabel">Verificar calificación enviada</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
            <form method="post" id="form_confirm" action="" class="needs-validation row" novalidate>
                {{ csrf_field() }}
                @method('put')
                <div class="form-row">
                  <label for="qualification">Calificación</label>
                  <input step="0.01" name="qualification" type="number" value="" required="required" class="form-control">
                </div>
                <div class="form-row">
                  <label for="kardex-qualification-status">Estatus de la calificación</label>
                  <select name="kardex-qualification-status" id="kardex-qualification-status" class="form-select">
                    @foreach(App\Models\Kardex::KARDEX_QUALIFICATION_STATUS as $status)
                      <option value="{{ $status }}">{{ strtoupper($status) }}</option>
                    @endforeach
                  </select>
                </div>
                <div id="confirm-container" class="d-none">
                  <div class="form-row">
                    <label for="approved-status">¿Aprobado?</label>
                    <select name="approved-status" id="approved-status" class="form-select">
                      <option selected="selected" value="null">Seleccione estatus aprobado o no aprobado</option>
                      @foreach(App\Models\Kardex::APPROVED_STATUS as $approved_status)
                        <option value="{{ $approved_status }}">{{ strtoupper($approved_status) }}</option>
                      @endforeach
                    </select>
                  </div>
                  
                  <div class="form-row">
                    <label for="approved-type">Tipo de aprobación?</label>
                    <select name="approved-type" id="approved-type" class="form-select">
                      @foreach(App\Models\Kardex::APPROVED_TYPE as $approved_type)
                        <option value="{{ $approved_type }}">{{ strtoupper($approved_type) }}</option>
                      @endforeach
                    </select>
                  </div>
                  
                </div>
                <div class="d-grid gap-2 mt-3">
                    <button type="submit" class="btn btn-info text-white" type="submit" >Listo</button>
                </div>
                <div class="d-grid gap-2 mt-3">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                </div>
            </form>
        </div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<script>

    const formCreate = document.querySelector("#form_confirm");
    formCreate.addEventListener('submit', function (event) {
        if (!formCreate.checkValidity()) {
          event.preventDefault()
          event.stopPropagation()
        }

        formCreate.classList.add('was-validated');
      }, false);

      var myModal = document.getElementById('modalConfirm')
      
      myModal.addEventListener('shown.bs.modal', function (e) {
        let button = e.relatedTarget;
        let kardex_data = JSON.parse(button.getAttribute('data-kardex'));
        let kardex_status_type = button.getAttribute("data-value");
        let route=button.getAttribute('data-save');
        formCreate.setAttribute('action',route );
        if(kardex_status_type == "confirmado"){
          document.querySelector("#confirm-container").classList.remove("d-none");
          formCreate['kardex-qualification-status'].value = kardex_status_type;
          formCreate['qualification'].value = parseFloat(kardex_data.prequalification).toFixed(2);
          formCreate['approved-type'].value = kardex_data.approved_type;
          formCreate['approved-status'].value = kardex_data.approved_status;
        }else{
          document.querySelector("#confirm-container").classList.add("d-none");
          formCreate['kardex-qualification-status'].value = kardex_status_type;
          formCreate['qualification'].value = parseFloat(kardex_data.prequalification).toFixed(2);
        }
      })
</script>