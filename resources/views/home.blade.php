@extends('layouts.dashboard')
@section('breadcrumb')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">Dashboard</h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">
              Inicio
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
@endsection
@section('content')
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
  <!-- ============================================================== -->
  <!-- Sales Cards  -->
  <!-- ============================================================== -->
  <div class="row">
    <!-- Column -->
    @hasanyrole('Administrator|School Control')
    <div class="col-md-6 col-lg-4 col-xlg-3">
      <a  class="link-light" href="{{ route('admin.students') }}">
      <div class="card card-hover">
        <div class="box bg-success text-center">
          <h1 class="font-light text-white">
            <i class="mdi mdi-face"></i>
          </h1>
          <h6 class="text-white">Estudiantes</h6>
        </div>
        </a>
      </div>
    </div>
    <!-- Column -->
    @endhasanyrole
    @hasanyrole('Administrator|School Control')
    <div class="col-md-6 col-lg-2 col-xlg-3">
      <a class="link-light"  href="{{ route('admin.users.index') }}">
        <div class="card card-hover">
          <div class="box bg-warning text-center">
            <h1 class="font-light text-white">
              <i class="mdi mdi-account"></i>
            </h1>
            <h6 class="text-white">Usuarios del sistema</h6>
          </div>
        </div>
      </a>
    </div>
    <!-- Column -->
    @endhasanyrole
    @hasanyrole('Administrator|School Control')
    <div class="col-md-6 col-lg-2 col-xlg-3">
      <a class="link-light" href="{{ route('admin.professors.index') }}">
        <div class="card card-hover">
          <div class="box bg-danger text-center">
            <h1 class="font-light text-white">
              <i class="mdi mdi-teacher">
                <svg style="width:24px;height:24px" viewBox="0 0 24 24">
                  <path fill="currentColor" d="M20,17A2,2 0 0,0 22,15V4A2,2 0 0,0 20,2H9.46C9.81,2.61 10,3.3 10,4H20V15H11V17M15,7V9H9V22H7V16H5V22H3V14H1.5V9A2,2 0 0,1 3.5,7H15M8,4A2,2 0 0,1 6,6A2,2 0 0,1 4,4A2,2 0 0,1 6,2A2,2 0 0,1 8,4Z"></path>
              </svg>
              </i>
            </h1>
            <h6 class="text-white">Docentes</h6>
          </div>
        </div>
      </a>
    </div>
    <!-- Column -->
    @endhasanyrole
    @hasanyrole('Administrator|School Control|Subdirection')
    <div class="col-md-6 col-lg-2 col-xlg-3">
      <a class="link-light" href="{{ route('admin.school-offer.list') }}">
      <div class="card card-hover">
        <div class="box bg-info text-center">
          <h1 class="font-light text-white">
            <i class="beaker-outline"><svg style="width:24px;height:24px" viewBox="0 0 24 24">
              <path fill="currentColor" d="M21 10H17V8L12.5 6.2V4H15V2H11.5V6.2L7 8V10H3C2.45 10 2 10.45 2 11V22H10V17H14V22H22V11C22 10.45 21.55 10 21 10M8 20H4V17H8V20M8 15H4V12H8V15M12 8C12.55 8 13 8.45 13 9S12.55 10 12 10 11 9.55 11 9 11.45 8 12 8M14 15H10V12H14V15M20 20H16V17H20V20M20 15H16V12H20V15Z"></path>
          </svg></i>
          </h1>
          <h6 class="text-white">Oferta escolar</h6>
        </div>
      </div>
      </a>
    </div>
    <!-- Column -->
    @endhasanyrole
    @hasanyrole('Administrator|School Control')
    <!-- Column -->
    <div class="col-md-6 col-lg-4 col-xlg-3">
      <a class="link-light" href="{{ route('admin.kardex.list') }}">
      <div class="card card-hover">
        <div class="box bg-danger text-center">
          <h1 class="font-light text-white">
            <i class="beaker-outline"><svg style="width:24px;height:24px" viewBox="0 0 24 24">
              <path fill="currentColor" d="M3,3H21V5A2,2 0 0,0 19,7V19A2,2 0 0,1 17,21H7A2,2 0 0,1 5,19V7A2,2 0 0,0 3,5V3M7,5V7H12V8H7V9H10V10H7V11H10V12H7V13H12V14H7V15H10V16H7V19H17V5H7Z" />
          </svg></i>
          </h1>
          <h6 class="text-white">Kardex</h6>
        </div>
      </div>
      </a>
    </div>
    <!-- Column -->
    @endhasanyrole
    @hasanyrole('Administrator|School Control')
    <div class="col-md-6 col-lg-2 col-xlg-3">
      <a class="link-light" href="{{ route('admin.signature.index') }}">
      <div class="card card-hover">
        <div class="box bg-info text-center">
          <h1 class="font-light text-white">
            <i class="mdi mdi-book-open-blank-variant">
              <svg style="width:24px;height:24px" viewBox="0 0 24 24">
                  <path fill="currentColor" d="M21,5C19.89,4.65 18.67,4.5 17.5,4.5C15.55,4.5 13.45,4.9 12,6C10.55,4.9 8.45,4.5 6.5,4.5C4.55,4.5 2.45,4.9 1,6V20.65C1,20.9 1.25,21.15 1.5,21.15C1.6,21.15 1.65,21.1 1.75,21.1C3.1,20.45 5.05,20 6.5,20C8.45,20 10.55,20.4 12,21.5C13.35,20.65 15.8,20 17.5,20C19.15,20 20.85,20.3 22.25,21.05C22.35,21.1 22.4,21.1 22.5,21.1C22.75,21.1 23,20.85 23,20.6V6C22.4,5.55 21.75,5.25 21,5M21,18.5C19.9,18.15 18.7,18 17.5,18C15.8,18 13.35,18.65 12,19.5V8C13.35,7.15 15.8,6.5 17.5,6.5C18.7,6.5 19.9,6.65 21,7V18.5Z" />
              </svg>
            </i>
          </h1>
          <h6 class="text-white">Asignaturas</h6>
        </div>
      </div>
      </a>
    </div>
    @endhasanyrole
    @hasanyrole('Student')
    <!-- Column -->
    <div class="col-md-6 col-lg-4 col-xlg-3">
      <a class="link-light" href="{{ route('kardex') }}">
      <div class="card card-hover">
        <div class="box bg-danger text-center">
          <h1 class="font-light text-white">
            <i class="beaker-outline"><svg style="width:24px;height:24px" viewBox="0 0 24 24">
              <path fill="currentColor" d="M3,3H21V5A2,2 0 0,0 19,7V19A2,2 0 0,1 17,21H7A2,2 0 0,1 5,19V7A2,2 0 0,0 3,5V3M7,5V7H12V8H7V9H10V10H7V11H10V12H7V13H12V14H7V15H10V16H7V19H17V5H7Z" />
          </svg></i>
          </h1>
          <h6 class="text-white">Kardex</h6>
        </div>
      </div>
      </a>
    </div>
    <!-- Column -->
    @endhasanyrole
    @hasanyrole('Professor')
    <!-- Column -->
    <div class="col-md-6 col-lg-4 col-xlg-3">
      <a class="link-light" href="{{ route('kardex.professor') }}">
      <div class="card card-hover">
        <div class="box bg-danger text-center">
          <h1 class="font-light text-white">
            <i class="beaker-outline"><svg style="width:24px;height:24px" viewBox="0 0 24 24">
              <path fill="currentColor" d="M3,3H21V5A2,2 0 0,0 19,7V19A2,2 0 0,1 17,21H7A2,2 0 0,1 5,19V7A2,2 0 0,0 3,5V3M7,5V7H12V8H7V9H10V10H7V11H10V12H7V13H12V14H7V15H10V16H7V19H17V5H7Z" />
          </svg></i>
          </h1>
          <h6 class="text-white">Kardex</h6>
        </div>
      </div>
      </a>
    </div>
    <!-- Column -->
    @endhasanyrole
    @hasanyrole('Administrator|School Control')
    <!-- Column -->
    <div class="col-md-6 col-lg-4 col-xlg-3">
      <a class="link-light" href="{{ route('admin.kardex.qualification') }}">
      <div class="card card-hover">
        <div class="box bg-danger text-center">
          <h1 class="font-light text-white">
            <i class="beaker-outline"><svg style="width:24px;height:24px" viewBox="0 0 24 24">
              <path fill="currentColor" d="M17.5 14.33C18.29 14.33 19.13 14.41 20 14.57V16.07C19.38 15.91 18.54 15.83 17.5 15.83C15.6 15.83 14.11 16.16 13 16.82V15.13C14.17 14.6 15.67 14.33 17.5 14.33M13 12.46C14.29 11.93 15.79 11.67 17.5 11.67C18.29 11.67 19.13 11.74 20 11.9V13.4C19.38 13.24 18.54 13.16 17.5 13.16C15.6 13.16 14.11 13.5 13 14.15M17.5 10.5C15.6 10.5 14.11 10.82 13 11.5V9.84C14.23 9.28 15.73 9 17.5 9C18.29 9 19.13 9.08 20 9.23V10.78C19.26 10.59 18.41 10.5 17.5 10.5M21 18.5V7C19.96 6.67 18.79 6.5 17.5 6.5C15.45 6.5 13.62 7 12 8V19.5C13.62 18.5 15.45 18 17.5 18C18.69 18 19.86 18.16 21 18.5M17.5 4.5C19.85 4.5 21.69 5 23 6V20.56C23 20.68 22.95 20.8 22.84 20.91C22.73 21 22.61 21.08 22.5 21.08C22.39 21.08 22.31 21.06 22.25 21.03C20.97 20.34 19.38 20 17.5 20C15.45 20 13.62 20.5 12 21.5C10.66 20.5 8.83 20 6.5 20C4.84 20 3.25 20.36 1.75 21.07C1.72 21.08 1.68 21.08 1.63 21.1C1.59 21.11 1.55 21.12 1.5 21.12C1.39 21.12 1.27 21.08 1.16 21C1.05 20.89 1 20.78 1 20.65V6C2.34 5 4.18 4.5 6.5 4.5C8.83 4.5 10.66 5 12 6C13.34 5 15.17 4.5 17.5 4.5Z" />
          </svg></i>
          </h1>
          <h6 class="text-white">Calificación</h6>
        </div>
      </div>
      </a>
    </div>
    <!-- Column -->
    @endhasanyrole
    @hasanyrole('Administrator|School Control')
    <!-- Column -->
    <div class="col-md-6 col-lg-4 col-xlg-3">
      <a class="link-light" href="{{ route('admin.career.index') }}">
      <div class="card card-hover">
        <div class="box bg-primary text-center">
          <h1 class="font-light text-white">
            <i class="beaker-outline"><svg style="width:24px;height:24px" viewBox="0 0 24 24">
              <path fill="currentColor" d="M18 10.5V6L15.89 7.06C15.96 7.36 16 7.67 16 8C16 10.21 14.21 12 12 12C9.79 12 8 10.21 8 8C8 7.67 8.04 7.36 8.11 7.06L5 5.5L12 2L19 5.5V10.5H18M12 9L10 8C10 9.1 10.9 10 12 10C13.1 10 14 9.1 14 8L12 9M14.75 5.42L12.16 4.1L9.47 5.47L12.07 6.79L14.75 5.42M12 13C14.67 13 20 14.33 20 17V20H4V17C4 14.33 9.33 13 12 13M12 14.9C9 14.9 5.9 16.36 5.9 17V18.1H18.1V17C18.1 16.36 14.97 14.9 12 14.9Z" />
          </svg></i>
          </h1>
          <h6 class="text-white">Carreras</h6>
        </div>
      </div>
      </a>
    </div>
    <!-- Column -->
    @endhasanyrole
    @hasanyrole('Administrator|School Control')
    <!-- Column -->
    <div class="col-md-6 col-lg-4 col-xlg-3">
      <a class="link-light" href="{{ route('admin.kardex.qualification-requests') }}">
      <div class="card card-hover">
        <div class="box bg-warning text-center">
          <h1 class="font-light text-white">
            <i class="beaker-outline"><svg style="width:24px;height:24px" viewBox="0 0 24 24">
              <path fill="currentColor" d="M4,2A2,2 0 0,0 2,4V14H4V4H14V2H4M8,6A2,2 0 0,0 6,8V18H8V8H18V6H8M20,12V20H12V12H20M20,10H12A2,2 0 0,0 10,12V20A2,2 0 0,0 12,22H20A2,2 0 0,0 22,20V12A2,2 0 0,0 20,10Z" />
          </svg></i>
          </h1>
          <h6 class="text-white">Solicitudes de aprobación</h6>
        </div>
      </div>
      </a>
    </div>
    <!-- Column -->
    @endhasanyrole
  </div>
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
@endsection