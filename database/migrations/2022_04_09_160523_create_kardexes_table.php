<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKardexesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kardexs', function (Blueprint $table) {
            $table->id();
            $table->double('credits', 10,2);
            $table->string('period'); //ej 2022-I 2022-II
            $table->string('matricula');
            $table->string('matricula_professor');
            $table->unsignedInteger('semester_id');
            $table->unsignedInteger('signature_id');
            $table->double('approved_minimum');
            $table->enum('status_kardex', ['En curso', 'No cursado', 'Cursado'])->nullable();
            $table->enum('approved_status', ['NA', 'A'])->nullable();
            $table->enum('approved_type', ['acreditacion', 'regularizacion'])->nullable();
            $table->float('prequalification', 10, 2)->nullable();
            $table->float('qualification', 10, 2)->nullable();
            $table->unsignedInteger('career_id');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create("kardex_logs", function(BluePrint $table){
            $table->id();
            $table->text("data_before");
            $table->text("data_after");
            $table->unsignedBigInteger("user_id");
            $table->unsignedBigInteger("kardex_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kardexs');
        Schema::dropIfExists('kardex_logs');
    }
}
