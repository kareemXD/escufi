<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMinuteReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('minute_reports', function (Blueprint $table) {
            $table->id();
            $table->string("plantel");
            $table->string("clave_centro");
            $table->unsignedBigInteger("matricula_professor");
            $table->unsignedBigInteger("school_period_code");
            $table->string("kardex_group");
            $table->string("kardex_turn");
            $table->unsignedBigInteger("career_id");
            $table->unsignedInteger("signature_id");
            $table->integer("semester_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('minute_reports');
    }
}
