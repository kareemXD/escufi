<?php

use App\Models\Signature;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SignatureModifyMigration extends Migration
{
    private $signatures_table;
    public function __construct(){
        $this->signatures_table = app(Signature::class)->getTable();
    }
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->signatures_table, function(Blueprint $table){
            $table->char('clave', 4)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->signatures_table, function(Blueprint $table){
            if (Schema::hasColumn($this->signatures_table, 'clave')) {
                $table->dropColumn('clave');
            }
        });
    }
}
