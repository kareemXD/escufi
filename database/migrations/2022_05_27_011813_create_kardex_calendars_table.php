<?php

use App\Models\KardexCalendar;
use App\Models\SchoolOfferCalendar;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKardexCalendarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kardex_calendars', function (Blueprint $table) {
            $table->id();
            $table->time('checkin');
            $table->time('checkout');
            $table->enum('day_of_week', KardexCalendar::DAYS_OF_WEEK);
            $table->unsignedBigInteger("kardex_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kardex_calendars');
    }
}
