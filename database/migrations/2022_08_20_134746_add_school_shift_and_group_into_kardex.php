<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSchoolShiftAndGroupIntoKardex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kardexs', function($table) {
            $table->string('school_shift_id', 1)->default("V");
            $table->unsignedBigInteger("school_group_id")->default(1);
            $table->double("assist_percent", 10, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kardexs', function($table) {
            $table->dropColumn('school_group_id');
            $table->dropColumn('school_shift_id');
            $table->dropColumn('assist_percent');
        });
    }
}
