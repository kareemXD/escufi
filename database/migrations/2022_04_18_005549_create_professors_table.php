<?php

use App\Models\Professor;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfessorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('professors', function (Blueprint $table) {
            $table->id();
            $table->string('matricula_professor')->unique();
            $table->string('paterno');
            $table->string('materno');
            $table->string('nombre');
            $table->date('fecha_nacimiento');
            $table->string('nss');
            $table->string('email');
            $table->string('curp');
            $table->boolean('active')->default(Professor::STATUS_ACTIVE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('professors');
    }
}
