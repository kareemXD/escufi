<?php

use App\Models\SchoolOfferCalendar;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolOfferCalendarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_offer_calendars', function (Blueprint $table) {
            $table->id();
            $table->time('checkin');
            $table->time('checkout');
            $table->enum('day_of_week', SchoolOfferCalendar::DAYS_OF_WEEK);
            $table->unsignedBigInteger("school_offer_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_offer_calendars');
    }
}
