<?php

use App\Models\SchoolPeriod as SchoolPeriodModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SchoolPeriod extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_periods', function (Blueprint $table) {
            $table->id();
            $table->char('code')->unique();
            $table->boolean("open")->default(SchoolPeriodModel::STATUS_OPEN);
            $table->timestamps();
        });

        DB::table('school_periods')->insert(["code" => env('CURRENT_SCHOOL_PERIOD')]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_periods');
    }
}
