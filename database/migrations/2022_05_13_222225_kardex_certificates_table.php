<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class KardexCertificatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kardex_certificates', function (Blueprint $table) {
            $table->id();
            $table->string('string_certificate');
            $table->string('school_period');
            $table->string("matricula");
            $table->enum('approved_type', ['acreditacion', 'regularizacion']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kardex_certificates');
    }
}
