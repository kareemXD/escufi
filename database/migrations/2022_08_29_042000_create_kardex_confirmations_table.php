<?php

use App\Models\KardexConfirmation;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKardexConfirmationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        #todo analizar tabla para que en control escolar sea mas facil confirmar calificaciones
        Schema::create('kardex_confirmations', function (Blueprint $table) {
            $table->id();
            $table->string("request_number")->unique();
            $table->text("description");
            $table->enum("qualification_status", KardexConfirmation::KARDEX_QUALIFICATION_STATUS);
            $table->string("matricula_professor");
            $table->unsignedBigInteger("user_id")->nullable(); //approver
            $table->timestamps();
            $table->foreign("user_id")->references('id')->on('users');
        });

        Schema::table("kardexs", function(Blueprint $table){
            $table->unsignedBigInteger("kardex_confirmation_id")->nullable();
            $table->foreign("kardex_confirmation_id")->references('id')->on('kardex_confirmations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("kardexs", function(Blueprint $table){
            $table->dropForeign(['kardex_confirmation_id']);
            $table->dropColumn('kardex_confirmation_id');
        });
        Schema::dropIfExists('kardex_confirmations');

    }
}
