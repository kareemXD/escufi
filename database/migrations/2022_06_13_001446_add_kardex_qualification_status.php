<?php

use App\Models\Kardex;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKardexQualificationStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kardexs', function($table) {
            $table->enum('kardex_qualification_status', Kardex::KARDEX_QUALIFICATION_STATUS)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kardexs', function($table) {
            $table->dropColumn('kardex_qualification_status');
        });
    }
}
