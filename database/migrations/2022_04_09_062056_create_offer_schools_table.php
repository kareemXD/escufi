<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfferSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_offers', function (Blueprint $table) {
            $table->id();
            $table->string('matricula_professor');
            $table->unsignedInteger('semester_id');
            $table->unsignedInteger('signature_id');
            $table->unsignedInteger('career_id');
            $table->string('school_shift_id');
            $table->unsignedInteger('school_group_id');
            $table->integer('credits');
            $table->double('approving_minimum', 10 , 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_offers');
    }
}
