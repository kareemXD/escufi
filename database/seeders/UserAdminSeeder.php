<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;

class UserAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(empty(User::find(1))){

            $role_admin = Role::create(['name' => 'Administrator']);
            $role_student = Role::create(['name' => 'Student']);
            $role_professor = Role::create(['name' => 'Professor']);
            $role_scolar_control = Role::create(['name' => '
            School Control']);
            $role_subdirection = Role::create(['name' => 'Subdirection']);
            $permission = Permission::create(['name' => 'all']);
            $permission->assignRole($role_admin);
            $password = env('PASSWORD_DEFAULT');
            $user = User::create([
                'name' => env("NOMBRE_DEFAULT"),
                'email' => env("EMAIL_DEFAULT"),
                'matricula' => env("MATRICULA_DEFAULT"),
                'password' => Hash::make($password),
                'type_user' => 'Administrador'
            ]);

            $user->assignRole('Administrator');
        }else{
            dd( "el usuario ya se registró jeje");
        }

    }
}
