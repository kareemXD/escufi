<?php 
namespace App\Repositories;

use App\Models\Student;
use App\Models\SchoolOffer;
use App\Models\SchoolOfferCalendar;

class SchoolOfferRepository{
    public function __construct(
        SchoolOffer $schoolOffer,
        SchoolOfferCalendar $schoolOfferCalendar
    ){
        $this->schoolOffer = $schoolOffer;
        $this->schoolOfferCalendar = $schoolOfferCalendar;
    }

    public function createSchoolOffer($data)
    {
        $schoolOffer = new SchoolOffer();
        $schoolOffer->matricula_professor = $data->{'create-professor'};
        $schoolOffer->semester_id = $data->{'create-semester-id'};
        $schoolOffer->signature_id = $data->{'create-signature'};
        $schoolOffer->career_id = $data->{'create-career-id'};
        $schoolOffer->credits = $data->{'create-credits'};
        $schoolOffer->approving_minimum = $data->{'create-approving-minimum'};
        $schoolOffer->school_shift_id = $data->{'create-school-shift-id'};
        $schoolOffer->school_group_id = $data->{'create-school-group-id'};
        $schoolOffer->save();

        if(!is_null($data->{'create-calendar-checkin'})){
            for ($i=0; $i < count($data->{'create-calendar-checkin'}); $i++) { 
                $createCheckin = $data->{'create-calendar-checkin'}[$i];
                $createCheckout = $data->{'create-calendar-checkout'}[$i];
                $createDayOfWeek = $data->{'create-calendar-day-of-week'}[$i];
                $this->schoolOfferCalendar->create([
                    'checkin' => $createCheckin,
                    'checkout' => $createCheckout,
                    'day_of_week' => $createDayOfWeek,
                    'school_offer_id' => $schoolOffer->id
                ]);
            }
        }

        $schoolOffer = $schoolOffer->fresh();
        return $schoolOffer;
    }

    public function updateSchoolOffer(SchoolOffer $schoolOffer, $data)
    {
        $schoolOffer->matricula_professor = $data->{'edit-professor'};
        $schoolOffer->semester_id = $data->{'edit-semester-id'};
        $schoolOffer->signature_id = $data->{'edit-signature'};
        $schoolOffer->career_id = $data->{'edit-career-id'};
        $schoolOffer->credits = $data->{'edit-credits'};
        $schoolOffer->approving_minimum = $data->{'edit-approving-minimum'};
        $schoolOffer->school_shift_id = $data->{'edit-school-shift-id'};
        $schoolOffer->school_group_id = $data->{'edit-school-group-id'};
        $schoolOffer->save();

        if(!is_null($data->{'edit-calendar-checkin'})){
            $editSchoolOfferCalendars = [];
            $schoolOffer->schoolOfferCalendars()->delete();
            for ($i=0; $i < count($data->{'edit-calendar-checkin'}); $i++) { 
                $editCheckin = $data->{'edit-calendar-checkin'}[$i];
                $editCheckin = $data->{'edit-calendar-checkout'}[$i];
                $createDayOfWeek = $data->{'edit-calendar-day-of-week'}[$i];
                $this->schoolOfferCalendar->create([
                    'checkin' => $editCheckin,
                    'checkout' => $editCheckin,
                    'day_of_week' => $createDayOfWeek,
                    'school_offer_id' => $schoolOffer->id
                ]);
            }
        }

        $schoolOffer = $schoolOffer->fresh();
        return $schoolOffer;
    }

    public function findSchoolOffersByStudent(Student $student)
    {
        return $this->schoolOffer->where("semester_id", $student->id_semestre)
                ->where('career_id', $student->id_carrera)
                ->with(["professor", "semester", "signature", "career", "schoolGroup", "schoolOfferCalendars" ])
                ->get();
    }
}