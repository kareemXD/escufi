<?php

namespace App\Repositories;

use App\Models\Kardex;
use App\Models\Signature;
use App\Models\MinuteReport;
use App\Models\SchoolPeriod;

class ReportRepository{

    public function __construct(
        MinuteReport $minuteReport,
        SchoolPeriod $schoolPeriod,
        Kardex $kardex
    ){
        $this->minuteReport = $minuteReport;
        $this->schoolPeriod = $schoolPeriod;
        $this->kardex = $kardex;
    }

    public function getMinuteReports(){
        $period_code = request()->{"period-code"};
        if($period_code == null){
            $period_code = $this->schoolPeriod->latest()->first()->code;
        }

        $minuteReports = $this->minuteReport->where("career_id", request("career-id"))
                                ->where("semester_id", request("semester-id"))
                                ->where("school_period_code", $period_code);

        if(request()->{"signature-id"}){
            $minuteReports->where("signature_id", request("signature-id"));
        }
        if(request()->{"matricula-professor"}){
            $minuteReports->where("matricula_professor", request("matricula-professor"));
        }
        
        return $minuteReports->paginate(25);
    }

    public function getListOfMaterias(){
        $period_code = request()->{"period-code"};
        if($period_code == null){
            $period_code = $this->schoolPeriod->latest()->first()->code;
        }


        $materialsKardex = $this->kardex->where("career_id", request("career-id"))
                    ->where("semester_id", request("semester-id"))
                    ->where("period", $period_code);
        
        $materialsKardex->select("period", "matricula_professor", "semester_id", "signature_id", "career_id")->groupBy("period", "matricula_professor", "semester_id", "signature_id", "career_id")->distinct();
       
        return $materialsKardex->get();
    }
}