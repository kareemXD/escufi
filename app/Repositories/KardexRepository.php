<?php
namespace App\Repositories;

use Str;
use Carbon\Carbon;
use App\Models\Career;
use App\Models\Kardex;
use App\Models\Student;
use App\Models\Semester;
use App\Models\KardexLog;
use App\Models\SchoolGroup;
use App\Models\SchoolOffer;
use App\Models\SchoolShift;
use App\Models\SchoolPeriod;
use App\Models\KardexConfirmation;
use Illuminate\Support\Facades\DB;
use App\Models\SchoolOfferCalendar;
use Illuminate\Database\Eloquent\Builder;

class KardexRepository{
    public function __construct(
        SchoolOffer $schoolOffer,
        SchoolOfferCalendar $schoolOfferCalendar,
        Kardex $kardex,
        KardexLog $kardexLog,
        SchoolPeriod $schoolPeriod,
        SchoolShift $schoolShift,
        SchoolGroup $schoolGroup,
        KardexConfirmation $kardexConfirmation,
        Student $student,
        Semester $semester,
        Career $career
    ){
        $this->schoolOffer = $schoolOffer;
        $this->schoolOfferCalendar = $schoolOfferCalendar;
        $this->kardex = $kardex;
        $this->kardexLog = $kardexLog;
        $this->schoolPeriod = $schoolPeriod;
        $this->kardexConfirmation = $kardexConfirmation;
        $this->student = $student;
        $this->schoolShift = $schoolShift;
        $this->schoolGroup = $schoolGroup;
        $this->career = $career;
        $this->semester = $semester;
    }

    public function storeKardex(Student $student, $data){

            $schoolOffers = $this->schoolOffer->whereIn("id", $data->{'school-offer'})->get();
            $kardexData = [];
            foreach($schoolOffers as $schoolOffer){
                $kardexData = [
                    "credits" => $schoolOffer->credits,
                    "period"  => $data->{"kardex-school-period"},
                    "matricula" => $student->matricula_alumno,
                    "matricula_professor" => $schoolOffer->matricula_professor,
                    "semester_id" => $student->id_semestre,
                    "signature_id" => $schoolOffer->signature_id,
                    "approved_minimum" => $schoolOffer->approving_minimum,
                    "status_kardex" => 'En curso',
                    "approved_status" => null,
                    "approved_type" => 'acreditacion',
                    "prequalification" => 0,
                    "qualification" => 0,
                    "career_id" => $schoolOffer->career_id,
                    'created_at' => Carbon::now()
                ];
                $kardex = $this->kardex->create($kardexData);
                $this->kardexLogCreate($kardex);
                $kardexData[] = $kardex;
            }
            return $kardexData;
    }
        /***
         * @author Kareem Lorenzana
         * @created 2024/09/11
         * @params
         * @desc get all active students witout kardex from current period
         * loop students by career, semester, shift, group
         * after loop first by career, then by semester, then by shift(V or M) and finally by group
         * @return int $studentsQuantity
        ****/
    public function storeDinamicKardex(){
        DB::beginTransaction();

        try{
            $currentPeriod = $this->schoolPeriod->getCurrentSchoolPeriod()->code;
            $careerIds = $this->career->active()->pluck("id");
            $semesterIds = $this->semester->pluck("id_semestre");
            $schoolShiftIds = $this->schoolShift->pluck("id_turno");
            $schoolGroupId = $this->schoolGroup->first()->id; ##todo review if going to grow the group
           // dd("todo good", ["currentPeriod" => $currentPeriod, "careerIds" => $careerIds, "semesterIds" => $semesterIds, "schoolShiftIds" => $schoolShiftIds]);
           //loop by ship
           $kardexData = collect([]);
           $totalStudents = 0;
           foreach($careerIds as $careerId)
           {
            //loop by semester
            foreach($semesterIds as $semesterId)
                {
                    foreach($schoolShiftIds as $schoolShiftId)
                    {
                        $students = $this->student->active()->whereDoesntHave("kardexs", function(Builder $query) use($currentPeriod){
                            $query->where('period', $currentPeriod);
                        })->where(function(Builder $q) use($careerId, $semesterId, $schoolShiftId){
                                 $q->where('turno', $schoolShiftId)->where("id_semestre", $semesterId)->where("id_carrera", $careerId);
                        })->with(["kardexs"])->get();

                        if($students->count()){
                            foreach($students as $student){
                                $schoolOffers = $this->schoolOffer->where(function(Builder $q) use($careerId, $semesterId, $schoolShiftId){
                                         $q->where('school_shift_id', $schoolShiftId)->where("semester_id", $semesterId)->where("career_id", $careerId);
                                })->get();
                                if($schoolOffers->count()){
                                    $totalStudents++;
                                }
                                foreach($schoolOffers as $schoolOffer){

                                    $kardexDataCurrent = [
                                        "credits" => $schoolOffer->credits,
                                        "period"  => $currentPeriod,
                                        "matricula" => $student->matricula_alumno,
                                        "matricula_professor" => $schoolOffer->matricula_professor,
                                        "semester_id" => $student->id_semestre,
                                        "signature_id" => $schoolOffer->signature_id,
                                        "approved_minimum" => $schoolOffer->approving_minimum,
                                        "status_kardex" => 'En curso',
                                        "approved_status" => null,
                                        "approved_type" => 'acreditacion',
                                        "prequalification" => 0,
                                        "qualification" => 0,
                                        "career_id" => $schoolOffer->career_id,
                                        'created_at' => Carbon::now()
                                    ];
                                    $kardex = $this->kardex->create($kardexDataCurrent);
                                    $this->kardexLogCreate($kardex);
                                    $kardexData[] = $kardexDataCurrent;
                                }
                            }
                        }
                    }
                }

            }
            $responseData = (object)[
                "kardexs" => $kardexData,
                "total_students" => $totalStudents,
                "message" => "Se asignaron correctamente el Kardex del periodo $currentPeriod a $totalStudents estudiantes."
            ];
            DB::commit();
            return $responseData;

        }catch(\Exception $e){
            DB::rollback();
            return $e;
        }
}

    /**
     * @author Kareem Leonardo Lorenzana Guizar
     * @created 2024/09/11
     * collection of id kardexs
     * store list of logs of created kardexs
     * return void
     */
    public function kardexLogCreateArray($kardexData){

        #todo add try catch
        $dataInsert = [];

        foreach($kardexData as $kardex){
            $dataInsert[] = [
                "data_before" => json_encode(null),
                "data_after" => json_encode($kardex),
                "user_id" => auth()->user()->id,
                "kardex_id" => $kardex->id
            ];
        }
        $this->kardexLog->insert($dataInsert);
    }

    public function kardexLogCreate(Kardex $kardex){
        $this->kardexLog->create([
            "data_before" => json_encode(null),
            "data_after" => json_encode($kardex),
            "user_id" => auth()->user()->id,
            "kardex_id" => $kardex->id
        ]);
    }

    public function kardexLog($data_before, Kardex $kardex){
        $this->kardexLog->create([
            "data_before" => json_encode($data_before),
            "data_after" => json_encode($kardex),
            "user_id" => auth()->user()->id,
            "kardex_id" => $kardex->id
        ]);
    }

    public function findKardexByStudent(Student $student){
        return $this->kardex->where("semester_id", $student->id_semestre)
            ->where('career_id', $student->id_carrera)
            ->where("matricula", $student->matricula_alumno)
            ->with(["professor", "semester", "signature", "career", "student"])
            ->get();
    }

    public function getPeriodWithKardex($code = null){
        $student = auth()->user()->student;
        if(is_null($code)){
            $schoolPeriod = $this->schoolPeriod->with(["kardexs" => function($query)use($student){
                return $query->where("matricula", $student->matricula_alumno);
            }])->latest()->first();

        }else{
            $schoolPeriod = $this->schoolPeriod->where("code", $code)->with(["kardexs" => function($query)use($student){
                return $query->where("matricula", $student->matricula_alumno);
            }])->first();
        }

        return $schoolPeriod;
    }
    public function getPeriodWithKardexProfessor($code = null){
        $professor = auth()->user()->professor;
        if(is_null($code)){
            $schoolPeriod = $this->schoolPeriod->with(["kardexs" => function($query)use($professor){
                return $query->where("matricula", $professor->matricula_professor);
            }])->latest()->first();

        }else{
            $schoolPeriod = $this->schoolPeriod->where("code", $code)->with(["kardexs" => function($query)use($professor){
                return $query->where("matricula", $professor->matricula_professor);
            }])->first();
        }

        return $schoolPeriod;
    }

    /**
     * @author Kareem Lorenzana
     * @created 2022-04-25
     * @description generate random string from kardex confirmation
     * @params
     * @return String
     */
    public function generateMatricula(){
        $kardexConfirm = $this->professor->latest()
        ->first();
        $random = Str::upper(Str::random(2));
        if(empty($professor)){
            $matricula = Professor::MATRICULA_PREFIX.$random.str_pad(1, 8, '0', STR_PAD_LEFT);
        }else{
            $nextId = $professor->id +1;
            $matricula = Professor::MATRICULA_PREFIX.$random.str_pad($nextId, 8, '0', STR_PAD_LEFT);
        }
        return $matricula;
    }

    /**
     * @author Kareem Lorenzana
     * @created 2022-04-25
     * @description generate random string from kardex confirmation
     * @params
     * @return String
     */
    public function generateRequestNumber(){
        $kardexConfirmation = $this->kardexConfirmation->latest()
        ->first();
        $random = Str::upper(Str::random(2));
        if(empty($kardexConfirmation)){
            $matricula = KardexConfirmation::REQUEST_PREFIX.$random.str_pad(1, 8, '0', STR_PAD_LEFT);
        }else{
            $nextId = $kardexConfirmation->id +1;
            $matricula = KardexConfirmation::REQUEST_PREFIX.$random.str_pad($nextId, 8, '0', STR_PAD_LEFT);
        }
        return $matricula;
    }

    public function getOpenQualifications()
    {
        return $this->kardexConfirmation->where(function($query){
            return $query->where("qualification_status", "revision");
        })->get();
    }

}
