<?php
namespace App\Repositories;

use App\Models\Professor;
use Illuminate\Support\Str;

class ProfessorRepository{

    public function __construct(
        Professor $professor
    ){
        $this->professor = $professor;
    }

    public function getAllProfessors(){
        return $this->professor->all();
    }

    public function getActiveProfessors(){
        return $this->professor->active()->get();
    }

    public function storeProfessor($data)
    {
        try {
            $professor = new Professor();
            $matricula = $this->generateMatricula();
            $professor->matricula_professor = $matricula;
            $professor->paterno = $data->paterno;
            $professor->materno = $data->materno;
            $professor->nombre = $data->nombre;
            $professor->fecha_nacimiento = $data->{'fecha-nacimiento'};
            $professor->email = $data->email;
            if(!is_null($data->nss)){
                $professor->nss = $data->nss;
            }else{
                $professor->nss = "";
            }
            if(!is_null($data->curp)){
                $professor->curp = $data->curp;
            }else{
                $professor->curp = "";
            }
            $professor->active = Professor::STATUS_ACTIVE;
            $professor->save();
            return $professor;
        } catch (\Exception $th) {
            return $th;
        }

    }

    /**
     * @author Kareem Lorenzana
     * @created 2022-04-25
     * @description generate random string from professor id using random string and prefix for unique data
     * @params
     * @return String
     */
    private function generateMatricula(){
        $professor = $this->professor->latest()
        ->first();
        $random = Str::upper(Str::random(2));
        if(empty($professor)){
            $matricula = Professor::MATRICULA_PREFIX.$random.str_pad(1, 8, '0', STR_PAD_LEFT);
        }else{
            $nextId = $professor->id +1;
            $matricula = Professor::MATRICULA_PREFIX.$random.str_pad($nextId, 8, '0', STR_PAD_LEFT);
        }
        return $matricula;
    }

    public function updateProfessor($data, Professor $professor)
    {
        try {
            $professor->paterno = $data->paterno;
            $professor->materno = $data->materno;
            $professor->nombre = $data->nombre;
            $professor->fecha_nacimiento = $data->{'fecha-nacimiento'};
            $professor->email = $data->email;
            if(!is_null($data->nss)){
                $professor->nss = $data->nss;
            }else{
                $professor->nss = "";
            }
            if(!is_null($data->curp)){
                $professor->curp = $data->curp;
            }else{
                $professor->curp = "";
            }
            $professor->save();
            return $professor;
        } catch (\Exception $th) {
            return $th;
        }
    }
}