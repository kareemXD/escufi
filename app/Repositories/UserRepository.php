<?php


namespace App\Repositories;

use App\Models\User;
use App\Models\Student;
use App\Models\Professor;
use Illuminate\Support\Facades\Hash;

class UserRepository{
    
    public function __construct(
        User $user,
        Student $student,
        Professor $professor
    ){
        $this->user = $user;
        $this->student = $student;
        $this->professor = $professor;
    }

    public function getActiveStudents()
    {
        return $this->student->active()->with(['career', 'semester'])->orderBy("matricula_alumno", "desc")->paginate();
    }

    public function getActiveStudentsQuery()
    {
        return $this->student->active()->with(['career', 'semester']);
    }

    public function getActiveUsers(){
        return $this->user->active()->paginate();
    }

    public function getAllUsersQuery(){
        return $this->user->paginate();
    }

    public function getDataResponseFromMatricula()
    {
        $matricula = request()->matricula;
        switch (request()->{'type-user'}) {
            case 'Profesor':
                $professor =  $this->professor->ofMatricula($matricula)->first();
                if(empty($professor)){
                    return new Exception("La matricula no es válida", 400);
                }
                return $professor;
                break;
            case 'Alumno':
                $student =  $this->student->with(['studentContact', 'semester'])->find($matricula);
                if(empty($student)){
                    return new Exception("La matricula no es válida", 400);
                }
                return $student;
                break;
                case 'Control Escolar':
                    return (object)[];
                break;
                case 'Subdireccion':
                    return (object)[];
                break;
            
            default:
                    return (object)[];
                break;
        }
    }

    public function createUser($data)
    {
        try{
            $user = new User();
            $user->matricula = $data->matricula;
            $user->name = $data->name;
            $user->email = $data->email;
            $user->password = Hash::make($data->password);
            $user->type_user = $data->{"type-user"};
            $user->save();
            $user->assignRolesOfType($data->{"type-user"});
    
            return $user;
        }catch(\Exception $e){
            return $e;
        }
        
    }

    public function updateUser($data, User $user){
        try{
            $user->name = $data->name;
            $user->email = $data->email;
            if($data->password != null){
                $user->password = Hash::make($data->password);
            }
            $user->save();
    
            return $user;
        }catch(\Exception $e){
            return $e;
        }
    }
}