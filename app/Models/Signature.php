<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Signature extends Model
{
    use HasFactory;
    const STATUS_ACTIVE = "A";
    const STATUS_INACTIVE = "I";

    protected $table = "cat_asignaturas";
    protected $fillable =["asignatura", "calificacion_aprovatoria", "activa", "clave"];

    protected $primaryKey = "id_asignatura";
    public $timestamps = false;
    public function scopeActive($query){
        return $query->where("activa", self::STATUS_ACTIVE);
    }

    public function kardexs(){
        return $this->hasMany(Kardex::class, "signature_id", "id_asignatura");
    }

}
