<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KardexConfirmation extends Model
{
    use HasFactory;
    const KARDEX_QUALIFICATION_STATUS = Kardex::KARDEX_QUALIFICATION_STATUS;
    const REQUEST_PREFIX = "REQ";

    protected $table = "kardex_confirmations";

    protected $fillable = [
        "request_number",
        "description",
        "qualification_status",
        "matricula_professor", //quien hace la solicitud
        "user_id", //quien acepta la solicitud,
        "qualification_status"
        ];

    public function kardexs(){
        return $this->hasMany(Kardex::class, "kardex_confirmation_id");
    }

    public function user(){
        return $this->belongsTo(User::class, "user_id");
    }

    public function professor(){
        return $this->belongsTo(Professor::class, "matricula_professor", "matricula_professor");
    }
}
