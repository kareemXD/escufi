<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SchoolOfferCalendar extends Model
{
    use HasFactory;
    const DAYS_OF_WEEK = [
        'Lunes',
        'Martes',
        'Miercoles',
        'Jueves',
        'Viernes',
        'Sabado'
    ];

    protected $table = "school_offer_calendars";
    protected $fillable = ["checkin", "checkout", "day_of_week","school_offer_id"];

    public function schoolOffer(){
        return $this->belongsTo(SchoolOffer::class, "school_offer_id");
    }

}
