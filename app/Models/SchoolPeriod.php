<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SchoolPeriod extends Model
{
    use HasFactory;
    const STATUS_OPEN = true;
    const STATUS_CLOSE = false;
    protected $table = "school_periods";
    protected $fillable = ["code"];

    public function getCurrentSchoolPeriod(){
        return $this->latest("id")->first();
    }

    public function kardexs(){
        return $this->hasMany(Kardex::class, "period", "code");
    }
}
