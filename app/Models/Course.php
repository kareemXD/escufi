<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;

    const STATUS_ACTIVE = "A";

    protected $table = "cat_asignaturas";
    protected $guarded = [];

    public $timestamps = false;

    protected $primaryKey = "id_asignatura";

}
