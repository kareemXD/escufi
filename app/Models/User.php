<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles;
    const STATUS_ACTIVE = true,
    STATUS_INACTIVE = false;
    const TYPE_USERS = ['Control Escolar', 'Alumno', 'Profesor', 'Subdireccion', 'Administrador'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'matricula',
        'active',
        'type_user'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function student()
    {
        return $this->belongsTo(Student::class, "matricula");
    }

    public function professor()
    {
        return $this->belongsTo(Professor::class, "matricula", "matricula_professor");
    }

    public function scopeActive($query)
    {
        return $query->where('active', self::STATUS_ACTIVE);
    }

    public function assignRolesOfType($type_user)
    {
        switch ($type_user) {
            case 'Control Escolar':
                $this->assignRole('School Control');
                break;
            case 'Alumno':
                $this->assignRole('Student');
                break;
            case 'Profesor':
                $this->assignRole('Professor');
                break;
            case 'Administrador':
                $this->assignRole('Administrator');
                break;
            case 'Subdireccion':
                $this->assignRole('Subdirection');
                break;
            
            default:
                $this->assignRole('Student');
                break;
        }
    }

}
