<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Student extends Model
{
    use HasFactory;

    const STATUS_ACTIVE = 1; //cuando ya renovó el semestre
    const STATUS_RENEW = 2; //cuando el alumno aun no renueva el semestre
    const TEMP_INACTVE = 3; // BAJA TEMPORAL
    const DEFINITIVE_INACTIVE = 4; //baja definitiva
    const BLOQUED = 5; // bloqueo por falta de pago
    const GRADUATE = 6; //el alumno ya esta graduado

    protected $table = "insc_datos_generales_alumno";
    protected $fillable = ["curp", "matricula_alumno", "matricula_auxiliar", "paterno", "materno", "nombre","grupo_sanguineo", "id_carrera", "status", "id_semestre", "fecha_registro"];

    protected $appends = ["url_kardex", "full_name"];
    public $timestamps = false;

    protected $primaryKey = "matricula_alumno";
    public $incrementing = false;

    // In Laravel 6.0+ make sure to also set $keyType
    protected $keyType = 'string';

    /**
     * Scope a query to only include active students
     * @author Kareem Lorenzana
     * @created 2022-04-08
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query){
        return $query->where('status', self::STATUS_ACTIVE);
    }

    /**
     * Get the semester of current student
     * @author Kareem Lorenzana
     * @create 2022-04-08
     * @params
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function semester()
    {
        return $this->belongsTo(Semester::class, "id_semestre");
    }

    /**
     * Get the carreer of current student
     * @author Kareem Lorenzana
     * @create 2022-04-08
     * @params
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function career()
    {
        return $this->belongsTo(Career::class, "id_carrera");
    }

    /**
     * Get the user of current student
     * @author Kareem Lorenzana
     * @create 2022-04-08
     * @params
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, "matricula_alumno", "matricula");
    }

    /**
     * Get the contact data of current student
     * @author Kareem Lorenzana
     * @create 2022-04-08
     * @params
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function studentContact()
    {
        return $this->belongsTo(StudentContact::class, "curp");
    }

    public function kardexs(){
        return $this->hasMany(Kardex::class,"matricula");
    }

    public function kardexsLastPeriod($period){
        return $this->hasMany(Kardex::class,"matricula")->where("period", $period);
    }


    /**
     * check if current student has Kardex registration
     * @author Kareem Lorenzana
     * @create 2022-04-08
     * @params
     * @return boolean
     */
    public function hasUser()
    {
        if(!empty($this->user)){
            return true;
        }
        return false;
    }

    public function hasKardex(){
        if(count($this->kardexs) > 0){
            return true;
        }
        return false;
    }

    public function getUrlKardexAttribute(){
        return route("admin.kardex.create", $this->matricula_alumno);
    }

    public function getFullNameAttribute(){
        return strtoupper($this->nombre." ".$this->paterno." ".$this->materno);
    }
}
