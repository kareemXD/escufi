<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SchoolShift extends Model
{
    use HasFactory;

    protected $table = "cat_turno_escolar";
    protected $fillable = ["turno"];

    public $timestamps = false;

    protected $primaryKey = "id_turno";
    public $incrementing = false;

    // In Laravel 6.0+ make sure to also set $keyType
    protected $keyType = 'string';


}
