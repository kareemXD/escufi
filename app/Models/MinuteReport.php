<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MinuteReport extends Model
{
    use HasFactory;

    protected $table = "minute_reports";
    protected $fillable = ["plantel", "clave_centro", "matricula_professor", "school_period_code", "kardex_group", "kardex_turn", "career_id", "signature_id", "semester_id"];

    public function signature(){
        return $this->belongsTo(Signature::class, "signature_id");
    }

    public function professor(){
        return $this->belongsTo(Professor::class, "matricula_professor", "matricula_professor");
    }

    public function semester(){
        return $this->belongsTo(Semester::class, "semester_id");
    }
}
