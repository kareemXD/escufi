<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Professor extends Model
{
    use HasFactory;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const MATRICULA_PREFIX = 'DOC';

    protected $table = 'professors';
    protected $fillable = ["matricula_professor", "paterno", "materno", "nombre", "fecha_nacimiento", "nss", "email", "curp", "active"];
    protected $appends = ["full_name"];

    public function scopeOfMatricula($query, $matricula){
        return $query->where("matricula_professor", $matricula);
    }

    public function scopeActive($query){
        return $query->where("active", self::STATUS_ACTIVE);
    }

    public function user(){
        return $this->belongsTo(User::class, 'matricula_professor', "matricula");
    }

    public function getFullNameAttribute(){
        return $this->nombre." ".$this->paterno;
    }

    public function signatures(){
        return $this->belongsToMany(Signature::class, "kardexs", "matricula_professor", "signature_id", "matricula_professor", "id_asignatura");
    }
}
