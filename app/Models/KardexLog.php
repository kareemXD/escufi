<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KardexLog extends Model
{
    use HasFactory;

    protected $table = "kardex_logs";
    protected $fillable = ["data_before" , "data_after", "user_id", "kardex_id"];
}
