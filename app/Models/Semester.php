<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Semester extends Model
{
    use HasFactory;

    protected $table = "cat_semestres";
    protected $fillable = ["semestre"];

    public $timestamps = false;

    protected $primaryKey = "id_semestre";
}
