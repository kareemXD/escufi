<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class SchoolOffer extends Model
{
    use HasFactory;

    protected $table = "school_offers";
    protected $fillable = ["matricula_professor", "semester_id", "signature_id", "career_id", "credits", "approving_minimum", "school_shift_id", "school_group_id"];

    public function professor()
    {
        return $this->belongsTo(Professor::class, "matricula_professor", "matricula_professor");
    }

    public function semester()
    {
        return $this->belongsTo(Semester::class, "semester_id", "id_semestre");
    }

    public function signature()
    {
        return $this->belongsTo(Signature::class, "signature_id", "id_asignatura");
    }

    public function career(){
        return $this->belongsTo(Career::class, "career_id");
    }

    public function schoolGroup(){
        return $this->belongsTo(SchoolGroup::class, "school_group_id");
    }

    public function schoolOfferCalendars(){
        return $this->hasMany(schoolOfferCalendar::class, "school_offer_id");
    }
}
