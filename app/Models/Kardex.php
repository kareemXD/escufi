<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Kardex extends Model
{
    use HasFactory, SoftDeletes;
    const STATUS_KARDEX = ['En curso', 'No cursado', 'Cursado'];
    const APPROVED_STATUS = ['NA', 'A'];
    const APPROVED_TYPE = ['acreditacion', 'regularizacion'];
    const KARDEX_QUALIFICATION_STATUS = ["confirmado", "rechazado", "revision"];
    protected $table = "kardexs";

    protected $fillable = ["credits", "period", "matricula", "matricula_professor", "semester_id", "signature_id", "approved_minimum", "status_kardex", "approved_status", "approved_type", "prequalification", "qualification", "career_id", "kardex_qualification_status", "school_shift_id", "school_group_id", "assist_percent", "kardex_confirmation_id"];


    public function semester(){
        return $this->belongsTo(Semester::class, "semester_id");
    }

    public function signature(){
        return $this->belongsTo(Signature::class, "signature_id");
    }

    public function career(){
        return $this->belongsTo(Career::class, "career_id");
    }

    public function student(){
        return $this->belongsTo(Student::class, "matricula");
    }

    public function professor(){
        return $this->belongsTo(Professor::class, "matricula_professor","matricula_professor");
    }
}
