<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Career extends Model
{
    const STATUS_ACTIVE = true;

    use HasFactory;
    protected $table = "careers";
    protected $fillable = ["name", "status"];

    /**
     * Scope a query to only include active careers
     * @author Kareem Lorenzana
     * @created 2022-04-08
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query){
        return $query->where('status', self::STATUS_ACTIVE);
    }
}
