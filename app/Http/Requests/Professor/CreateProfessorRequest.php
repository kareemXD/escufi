<?php

namespace App\Http\Requests\Professor;

use Illuminate\Foundation\Http\FormRequest;

class CreateProfessorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'paterno' => 'required|min:3|max:100',
            'materno' => 'required|min:3|max:100',
            'nombre' => 'required|min:3|max:100',
            'email' => 'required|email|min:3|max:100|unique:users,email|unique:professors,email',
            'nss' => 'nullable|min:3|max:16',
            'fecha-nacimiento' => 'date|required',
            'curp' => 'nullable|max:18|min:18'
        ];
    }
}
