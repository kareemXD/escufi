<?php

namespace App\Http\Requests;

use App\Models\User;
use App\Models\Student;
use App\Models\Professor;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class VerifyUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch (request()->{'type-user'}) {
            case 'Profesor':
                $tableProfessor = app(Professor::class)->getTable();
                return [
                    'type-user' => [
                        'required',
                        Rule::in(User::TYPE_USERS),
                    ],
                    'matricula' => "required|exists:$tableProfessor,matricula_professor|unique:users,matricula"
                ];
                break;
            case 'Alumno':
                $studentTable = app(Student::class)->getTable();
                return [
                    'type-user' => [
                        'required',
                        Rule::in(User::TYPE_USERS),
                    ],
                    'matricula' => "required|exists:$studentTable,matricula_alumno|unique:users,matricula",
                    
                ];
                break;
            
            default:
                return [
                    'type-user' => [
                        'required',
                        Rule::in(User::TYPE_USERS),
                    ],
                    'matricula' => 'unique:users,matricula'
                ];
                break;
        }
    }

}
