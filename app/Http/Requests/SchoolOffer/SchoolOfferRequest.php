<?php

namespace App\Http\Requests\SchoolOffer;

use App\Models\Career;
use App\Models\Semester;
use App\Models\SchoolGroup;
use App\Models\SchoolShift;
use Illuminate\Foundation\Http\FormRequest;

class SchoolOfferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(!is_null(request()->{'semester-selected'})){
            $career_table = app(Career::class)->getTable();
            $semester_table = app(Semester::class)->getTable();
            $schoolShift_table = app(SchoolShift::class)->getTable();
            $schoolGroup_table = app(SchoolGroup::class)->getTable();
            return [
                'career-selected' => "required|exists:$career_table,id",
                'semester-selected' => "required|exists:$semester_table,id_semestre",
                'school-shift-selected' => "required|exists:$schoolShift_table,id_turno",
                'school-group-selected' => "required|exists:$schoolGroup_table,id"
            ];
        }
        return [
            //
        ];
    }
}
