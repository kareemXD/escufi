<?php

namespace App\Http\Requests\SchoolOffer;

use App\Models\Career;
use App\Models\Semester;
use App\Models\Professor;
use App\Models\Signature;
use App\Models\SchoolGroup;
use App\Models\SchoolShift;
use Illuminate\Foundation\Http\FormRequest;

class StoreSchoolOfferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $career_table = app(Career::class)->getTable();
        $semester_table = app(Semester::class)->getTable();
        $schoolShift_table = app(SchoolShift::class)->getTable();
        $schoolGroup_table = app(SchoolGroup::class)->getTable();
        $professor_table = app(Professor::class)->getTable();
        $signature_table = app(Signature::class)->getTable();
        return [
            'create-career-id' => "required|exists:$career_table,id",
            'create-semester-id' => "required|exists:$semester_table,id_semestre",
            'create-school-shift-id' => "required|exists:$schoolShift_table,id_turno",
            'create-school-group-id' => "required|exists:$schoolGroup_table,id",
            'create-professor' => "required|exists:$professor_table,matricula_professor",
            'create-signature' => "required|exists:$signature_table,id_asignatura",
            'create-approving-minimum' => 'required|numeric',
            'create-credits' => 'required|numeric'
        ];
    }
}
