<?php

namespace App\Http\Requests\SchoolOffer;

use App\Models\Career;
use App\Models\Semester;
use App\Models\Professor;
use App\Models\Signature;
use App\Models\SchoolGroup;
use App\Models\SchoolShift;
use Illuminate\Foundation\Http\FormRequest;

class UpdateSchoolOfferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $career_table = app(Career::class)->getTable();
        $semester_table = app(Semester::class)->getTable();
        $schoolShift_table = app(SchoolShift::class)->getTable();
        $schoolGroup_table = app(SchoolGroup::class)->getTable();
        $professor_table = app(Professor::class)->getTable();
        $signature_table = app(Signature::class)->getTable();
        return [
            'edit-career-id' => "required|exists:$career_table,id",
            'edit-semester-id' => "required|exists:$semester_table,id_semestre",
            'edit-school-shift-id' => "required|exists:$schoolShift_table,id_turno",
            'edit-school-group-id' => "required|exists:$schoolGroup_table,id",
            'edit-professor' => "required|exists:$professor_table,matricula_professor",
            'edit-signature' => "required|exists:$signature_table,id_asignatura",
            'edit-approving-minimum' => 'required|numeric',
            'edit-credits' => 'required|numeric'
        ];
    }
}
