<?php

namespace App\Http\Requests;

use App\Models\Kardex;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class KardexQualificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'prequalification' => 'required|min:0',
            'assist_percent' => 'required|min:0',
            'approved_status' => Rule::in(Kardex::APPROVED_STATUS)
        ];
    }
}
