<?php

namespace App\Http\Requests\User;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(request()->password == null){

            return [
                'name' => 'required|min:3|max:100',
                'email' => 'required|email|min:3|max:100',
            ];
        }
        return [
            'name' => 'required|min:3|max:100',
            'email' => 'required|email|min:3|max:100',
            'password' => 'required|min:3|max:100:',
            'password-confirmation' => 'required_with:password|same:password'
        ];
    }
}
