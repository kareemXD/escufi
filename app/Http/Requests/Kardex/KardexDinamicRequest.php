<?php

namespace App\Http\Requests\Kardex;

use Illuminate\Foundation\Http\FormRequest;

class KardexDinamicRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $schoolOffer_table = app(SchoolOffer::class)->getTable();
        $schoolPeriod_table = app(SchoolPeriod::class)->getTable();
        return [];
        return [
            'school-offer' => 'required|array',
            'school-offer.*' => "required|exists:$schoolOffer_table,id",
            'kardex-school-period' => "required|exists:$schoolPeriod_table,code"
        ];
    }

    public function messages(){
        return ['school-offer.required' => 'Debe tener al menos una materia seleccionada para el periodo.'];
    }
}
