<?php

namespace App\Http\Requests\Kardex;

use App\Models\Kardex;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class KardexConfirmRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'qualification' => 'required',
            'kardex-qualification-status' => [
                'required', 
                Rule::in(Kardex::KARDEX_QUALIFICATION_STATUS)
            ],
            'approved-type' => [
                'required',
                Rule::in(Kardex::APPROVED_TYPE),
            ],
            'approved-status' => [
                'nullable',
                'required',
                Rule::in(Kardex::APPROVED_STATUS),
            ],
        ];
    }
}
