<?php

namespace App\Http\Controllers;

use App\Models\Career;
use App\Models\Kardex;
use App\Models\Semester;
use App\Models\Professor;
use App\Models\Signature;
use App\Models\SchoolGroup;
use App\Models\SchoolShift;
use App\Models\SchoolPeriod;
use Illuminate\Http\Request;
use App\Repositories\ReportRepository;
use PhpOffice\PhpWord\TemplateProcessor;

class ReportController extends Controller
{

    public function __construct(
        ReportRepository $reportRepository,
        Career $career,
        Professor $professor,
        SchoolPeriod $schoolPeriod,
        Semester $semester,
        Kardex $kardex
    ){
        $this->middleware("auth");
        $this->reportRepository = $reportRepository;
        $this->career = $career;
        $this->professor = $professor;
        $this->schoolPeriod = $schoolPeriod;
        $this->semester = $semester;
        $this->kardex = $kardex;
    }
    public function reportSignature(){
        $templateProcessor = new TemplateProcessor('templates/ACTA APRENDIZAJE EN EL SERVICIO.docx');
        //datos del encabezado
        $templateProcessor->setValue('plantel', config('escufi.plantel'));
        $templateProcessor->setValue('career_name', 'LICENCIATURA EN EDUCACIÓN FÍSICA.');
        $templateProcessor->setValue('clave_centro', config('escufi.clave_centro'));
        $templateProcessor->setValue('signature_name', 'APRENDIZAJE EN EL SERVICIO');
        $templateProcessor->setValue('signature_key', 'APS1');
        $templateProcessor->setValue('semester_semestre', 'OCTAVO');
        $formatterES = new \NumberFormatter("es-ES", \NumberFormatter::SPELLOUT);
        $total_students = strtoupper($formatterES->format(20));
        $templateProcessor->setValue('total_students', $total_students);
        $templateProcessor->setValue('kardex_ac', 'X');
        $templateProcessor->setValue('kardex_re', '');
        $templateProcessor->setValue('kardex_group', 'ÚNICO');
        $templateProcessor->setValue('kardex_turn', 'DISCONTINUO');
        //inicia llenado de datos
        $formatterES = new \NumberFormatter("es-ES", \NumberFormatter::SPELLOUT);
        $qualification_letter = strtoupper($formatterES->format(10));
        $formatterES = new \NumberFormatter("es-ES", \NumberFormatter::SPELLOUT);
        $qualification_letter2 = strtoupper($formatterES->format(9.5));
        $values = [
            ['kardex_row' => 1,
             'kardex_curp' => 'LOGK94SF410HJCRZR00',
             'kardex_paterno' => 'LORENZANA',
             'kardex_materno' => 'GUIZAR',
             'kardex_nombre' => 'KAREEM LORENZANA',
             'kardex_qualification' => '10',
             'kardex_qualification_letter' => $qualification_letter,
             'kardex_acreditado' => 'X',
             'kardex_na' => '',
             'kardex_assist_percent' => '95%'
            ],
            ['kardex_row' => 1,
             'kardex_curp' => 'LOGK940410HJCRZR00',
             'kardex_paterno' => 'LORENZANA',
             'kardex_materno' => 'GUIZAR',
             'kardex_nombre' => 'KAREEM LORENZANA',
             'kardex_qualification' => '9.5',
             'kardex_qualification_letter' => $qualification_letter2,
             'kardex_acreditado' => 'X',
             'kardex_na' => '',
             'kardex_assist_percent' => '95%'
            ],
            ['kardex_row' => 1,
             'kardex_curp' => 'LOGK940410HJCRZR00',
             'kardex_paterno' => 'LORENZANA',
             'kardex_materno' => 'GUIZAR',
             'kardex_nombre' => 'KAREEM LORENZANA',
             'kardex_qualification' => '10',
             'kardex_qualification_letter' => $qualification_letter,
             'kardex_acreditado' => 'X',
             'kardex_na' => '',
             'kardex_assist_percent' => '95%'
            ],
        ];
        // fin
        $templateProcessor->cloneRowAndSetValues('kardex_row', $values);

        setlocale(LC_ALL,"spanish");
        $string = date('d/m/Y');
        $date = \DateTime::createFromFormat("d/m/Y", $string);
        $month = strtoupper(strftime('%B', $date->getTimestamp()));
        $day = date('d');
        $year = date('Y');
        if($day > 1){
            $date_text = "LA PAZ, BAJA CALIFORNIA SUR, A LOS $day DIAS DEL MES DE $month DEL AÑO $year.";
        }else{
            $date_text = "LA PAZ, BAJA CALIFORNIA SUR, AL $day DIA DEL MES DE $month DEL AÑO $year.";
        }
        $templateProcessor->setValue('information_date', $date_text);
        $templateProcessor->setValue('docente', 'RITA ADRIANA FLORES HERNÁNDEZ');
        $templateProcessor->setValue('acta_numero', '048/2021-2022');

        $wordDocumentFile = $templateProcessor->save();

        $headers = [
            'Content-Type' => 'application/msword',
            'Cache-Control' => 'max-age=0'
        ];

        return response()->download($wordDocumentFile, 'result.docx', $headers);
    }

    public function listOfReports(){
        $listOfMaterias = $this->reportRepository->getListOfMaterias();
        $careers = $this->career->all();
        $professors = $this->professor->all();
        $schoolPeriods = $this->schoolPeriod->get();
        $semesters = $this->semester->all();
        return view("admin.minute-report.list", compact("listOfMaterias", "careers", "professors", "schoolPeriods", "semesters"));
    }

    public function printReport(Career $career, Signature $signature, Professor $professor, $period, Semester $semester, SchoolShift $schoolShift, SchoolGroup $schoolGroup){
        $templateProcessor = new TemplateProcessor('templates/ACTA APRENDIZAJE EN EL SERVICIO.docx');
        //datos del encabezado
        $kardexInfo = $this->kardex->where("career_id", $career->id)
        ->where("semester_id", $semester->id_semestre)
        ->where("period", $period)->where("signature_id", $signature->id_asignatura)
        ->where("matricula_professor", $professor->matricula_professor)
        ->where("school_shift_id", $schoolShift->id_turno)
        ->where("school_group_id", $schoolGroup->id);

        $templateProcessor->setValue('plantel', config('escufi.plantel'));
        $templateProcessor->setValue('career_name', $career->name);
        $templateProcessor->setValue('clave_centro', config('escufi.clave_centro'));
        $templateProcessor->setValue('signature_name', $signature->asignatura);
        $templateProcessor->setValue('signature_key', $signature->code);
        $templateProcessor->setValue('semester_semestre', $semester->semester);
        $formatterES = new \NumberFormatter("es-ES", \NumberFormatter::SPELLOUT);
        $total_count = $kardexInfo->get()->count();
        $total_students = strtoupper($formatterES->format($total_count));
        $templateProcessor->setValue('total_students', $total_students);
        $acreditacion = "";
        $recursado = "";
        if(request()->re){
            $recursado = "X";
        }else{
            $acreditacion = "X";
        }
        $templateProcessor->setValue('kardex_ac', $acreditacion);
        $templateProcessor->setValue('kardex_re', $recursado);
        $templateProcessor->setValue('kardex_group', strtoupper($schoolGroup->name));
        $templateProcessor->setValue('kardex_turn', strtoupper($schoolShift->turno));
        $values = [];

        //inicia llenado de datos
        foreach($kardexInfo->get() as $index => $kardexData){
            $formatterES = new \NumberFormatter("es-ES", \NumberFormatter::SPELLOUT);
            $qualification_letter = strtoupper($formatterES->format($kardexData->qualification));
            $values[] = [
                'kardex_row' => $index +1,
                'kardex_curp' => $kardexData->student->curp,
                'kardex_paterno' => $kardexData->student->paterno,
                'kardex_materno' => $kardexData->student->materno,
                'kardex_nombre' => $kardexData->student->nombre,
                'kardex_qualification' => $kardexData->qualification,
                'kardex_qualification_letter' => $qualification_letter,
                'kardex_acreditado' => $kardexData->approved_status == "A"? 'X': '',
                'kardex_na' => $kardexData->approved_status == "A"? '': 'X',
                'kardex_assist_percent' => $kardexData->assist_percent."%"
            ];
        }

        // fin
        $templateProcessor->cloneRowAndSetValues('kardex_row', $values);

        setlocale(LC_ALL,"spanish");
        $string = date('d/m/Y');
        $date = \DateTime::createFromFormat("d/m/Y", $string);
        $month = strtoupper(strftime('%B', $date->getTimestamp()));
        $day = date('d');
        $year = date('Y');
        if($day > 1){
            $date_text = "LA PAZ, BAJA CALIFORNIA SUR, A LOS $day DIAS DEL MES DE $month DEL AÑO $year.";
        }else{
            $date_text = "LA PAZ, BAJA CALIFORNIA SUR, AL $day DIA DEL MES DE $month DEL AÑO $year.";
        }
        $templateProcessor->setValue('information_date', $date_text);
        $templateProcessor->setValue('docente', $professor->full_name);
        $templateProcessor->setValue('acta_numero', '048/2021-2022');

        $wordDocumentFile = $templateProcessor->save();

        $headers = [
            'Content-Type' => 'application/vnd.ms-excel',
            'Cache-Control' => 'max-age=0'
        ];

        return response()->download($wordDocumentFile, 'result.docx', $headers);
    }

}
