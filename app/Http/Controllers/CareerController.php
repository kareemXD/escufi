<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\Career;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateCareerRequest;

class CareerController extends Controller
{
    public function __construct(
        Career $career
    ){
        $this->career = $career;
    }
    public function index(){
        if (request()->ajax()) {
            $careers = $this->career->where("id", ">", 0);
            return DataTables::of($careers)->addColumn('action', function($row){
                return '<a class="btn btn-primary" href="'.route('admin.career.edit', $row).'"><i class="mdi mdi-account-edit text-white"></i></a>';
            })
            ->rawColumns(['action'])->make(true);
        }
        $careers = $this->career->paginate();
        return view('admin.career.index', compact('careers'));
    }

    public function edit(Career $career){
        return view("admin.career.edit", compact('career'));
    }

    public function update(Career $career, UpdateCareerRequest $request){
        try{
            $career->name =  ($request->{"career-name"});

            $career->save();

            return redirect()->back()->with("success", "Carrera modificada correctamente.");
        }catch(Exception $e){
            return redirect()->back()->withErrors("Hubo un error al modificar, intente mas tarde")->withInput($request->all());
        }
    }

    public function create(){
        return view("admin.career.create");
    }

    public function store(UpdateCareerRequest $request){
        try{
            $career = new Career;
            $career->name =  ($request->{"career-name"});

            $career->save();
            return redirect()->back()->with("success", "Carrera registrada correctamente.");
        }catch(Exception $e){
            return redirect()->back()->withErrors("Hubo un error al registrar, intente mas tarde")->withInput($request->all());
        }
    }
}
