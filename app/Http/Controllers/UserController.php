<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\User;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Http\Requests\VerifyUserRequest;
use App\Http\Requests\User\UserCreateRequest;
use App\Http\Requests\User\UserUpdateRequest;

class UserController extends Controller
{
    public function __construct(
        UserRepository $userRepository
    ){
        $this->userRepository = $userRepository;
        $this->middleware('auth');
    }

    public function index()
    {
                
        if (request()->ajax()) {
            $users = $this->userRepository->getAllUsersQuery();
            return Datatables::of($users)->addColumn('action', function($row){
                if($row->hasKardex()){
                    return '<a href="javascript:void(0)" class="edit btn btn-secondary btn-sm">Modificar usuario</a>';
                }

                return '<a href="javascript:void(0)" class="edit btn btn-primary btn-sm">Agregar Kardex</a>';
        })
        ->rawColumns(['action'])->make(true);
        }

        $users = $this->userRepository->getAllUsersQuery();
        return view('admin.user.index', compact('users'));
    }

    public function students()
    {
        
        if (request()->ajax()) {
            $students = $this->userRepository->getActiveStudentsQuery();
            return Datatables::of($students)->addColumn('action', function($row){
                return '<a href="'.route('admin.kardex.create',$row).'" class="edit btn btn-info btn-block">Kardex</a>';
        })
        ->rawColumns(['action'])->make(true);
        }

        $students = $this->userRepository->getActiveStudents();
        return view('admin.students', compact('students'));
    }

    public function create()
    {
        return view('admin.user.create');
    }

    public function verifyType(VerifyUserRequest $request){
        $response = $this->userRepository->getDataResponseFromMatricula();
        if($response instanceof Exception){
            return response($response->getMessage(), $response->getCode())->json();
        }

        return response()->json($response, parent::SUCCESS_RESPONSE);
        
    }

    public function store(UserCreateRequest $request)
    {
        $user = $this->userRepository->createUser($request);
        if($user instanceof Exception){
            return redirect()->back()->withErrors($user->getMessage())->withInput($request->all());
        }

        return redirect()->back()->withSuccess("Usuario registrado de manera correcta");
    }

    public function changeStatus(User $user){
        if(auth()->user()->id == $user->id){
            return response()->json("No puedes modificar tu propio usuario", 422);
        }elseif($user->type_user == 'Administrador'){
            return response()->json("Accion no permitida", 422);
        }
        try{
            $user->active = !$user->active;
            $user->save();
            $user->fresh();
        }catch(Exception $e){
            return response($e->getMessage(), $e->getCode())->json();
        }

        return response()->json($user, parent::SUCCESS_RESPONSE);
    }

    public function edit(User $user)
    {
        return view('admin.user.edit', compact('user'));
    }

    public function update(UserUpdateRequest $request, User $user){
        $userUpdate = $this->userRepository->updateUser($request, $user);
        if($userUpdate instanceof Exception){
            return redirect()->back()->withErrors($userUpdate->getMessage())->withInput($request->all());
        }

        return redirect()->back()->withSuccess("Usuario modificado de manera correcta");
    }
}
