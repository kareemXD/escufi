<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\Signature;
use Illuminate\Http\Request;
use App\Http\Requests\Signature\UpdateSignatureRequest;

class SignatureController extends Controller
{
    public function __construct(
        Signature $signature
    ){
        $this->signature = $signature;
    }
    public function index(){
        if (request()->ajax()) {
            $signatures = $this->signature->where("id_asignatura", ">", 0);
            return DataTables::of($signatures)->addColumn('action', function($row){
                return '<a class="btn btn-primary" href="'.route('admin.signature.edit', $row).'"><i class="mdi mdi-account-edit text-white"></i></a>';
            })->editColumn("activa", function(Signature $signature){
                
                return view('partials.datatables.signature.change-status', compact('signature'));
            })
            ->rawColumns(['action'])->make(true);
        }

        $signatures = $this->signature->paginate(25);
        return view('admin.signature.index', compact('signatures'));
    }

    public function toggleStatus(Signature $signature){
        try{
            if($signature->activa == Signature::STATUS_ACTIVE){
                $signature->activa = Signature::STATUS_INACTIVE;
            }else{
                $signature->activa = Signature::STATUS_ACTIVE;
            }
    
            $signature->save();
        }catch(Exception $e){
            return response($e->getMessage(), $e->getCode())->json();
        }


        return response()->json($signature, parent::SUCCESS_RESPONSE);
    }

    public function edit(Signature $signature){
        return view("admin.signature.edit", compact('signature'));
    }

    public function update(Signature $signature, UpdateSignatureRequest $request){
        try{
            $signature->asignatura =  strtoupper($request->asignatura);
            $signature->calificacion_aprovatoria = $request->{"calificacion-aprobatoria"};
            if($request->clave){
                $signature->clave = strtoupper($request->clave);
            }
            $signature->save();

            return redirect()->back()->with("success", "Asignatura modificada correctamente.");
        }catch(Exception $e){
            return redirect()->back()->withErrors("Hubo un error al modificar, intente mas tarde")->withInput($request->all());
        }
    }

    public function create(){
        return view("admin.signature.create");
    }

    public function store(UpdateSignatureRequest $request){
        try{
            $signature = new Signature();
            $signature->asignatura = strtoupper($request->asignatura);
            $signature->calificacion_aprovatoria = $request->{"calificacion-aprobatoria"};
            $signature->activa = Signature::STATUS_ACTIVE;
            if($request->clave){
                $signature->clave = strtoupper($request->clave);
            }
            $signature->save();

            return redirect()->back()->with("success", "Asignatura registrada correctamente.");
        }catch(Exception $e){
            return redirect()->back()->withErrors("Hubo un error al registrar, intente mas tarde")->withInput($request->all());
        }
    }
}
