<?php

namespace App\Http\Controllers;

use App\Models\Professor;
use Illuminate\Http\Request;
use App\Repositories\ProfessorRepository;
use App\Http\Requests\Professor\CreateProfessorRequest;
use App\Http\Requests\Professor\UpdateProfessorRequest;

class ProfessorController extends Controller
{
    public function __construct(ProfessorRepository $professorRepository){
        $this->professorRepository = $professorRepository;
        $this->middleware("auth");
    }

    public function index()
    {
        $professors = $this->professorRepository->getAllProfessors();
        return view('admin.professor.index', compact('professors'));
    }

    public function create()
    {
        return view('admin.professor.create');
    }

    public function store(CreateProfessorRequest $request)
    {
        $professor = $this->professorRepository->storeProfessor($request);
        if($professor instanceof Exception){
            return redirect()->back()->withErrors($professor->getMessage())->withInput($request->all());
        }

        return redirect()->back()->withSuccess("Profesor dado de alta de manera correcta");
    }

    public function edit(Professor $professor){
        return view('admin.professor.edit', compact('professor'));
    }

    public function update(UpdateProfessorRequest $request, Professor $professor){
        $professorUpdated = $this->professorRepository->updateProfessor($request, $professor);
        if($professorUpdated instanceof Exception){
            return redirect()->back()->withErrors($professorUpdated->getMessage())->withInput($request->all());
        }

        return redirect()->back()->withSuccess("Profesor modificado de manera correcta");
    }
}
