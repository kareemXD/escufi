<?php

namespace App\Http\Controllers;

use App\Models\Career;
use App\Models\Kardex;
use App\Models\Student;
use App\Models\Semester;
use App\Models\Signature;
use App\Models\SchoolGroup;
use App\Models\SchoolOffer;
use App\Models\SchoolShift;
use App\Models\SchoolPeriod;
use Illuminate\Http\Request;
use App\Models\KardexConfirmation;
use App\Repositories\KardexRepository;
use App\Repositories\ProfessorRepository;
use App\Repositories\SchoolOfferRepository;
use Illuminate\Database\Eloquent\Collection;
use App\Http\Requests\Kardex\KardexStoreRequest;
use App\Http\Requests\KardexQualificationRequest;
use App\Http\Requests\Kardex\KardexConfirmRequest;
use App\Http\Requests\SchoolOffer\SchoolOfferRequest;
use App\Http\Requests\SchoolOffer\StoreSchoolOfferRequest;
use App\Http\Requests\SchoolOffer\UpdateSchoolOfferRequest;

class KardexController extends Controller
{
    public function __construct(
        Semester $semester,
        Career $career,
        SchoolOffer $schoolOffer,
        ProfessorRepository $professorRepository,
        Signature $signature,
        SchoolShift $schoolShift,
        SchoolGroup $schoolGroup,
        SchoolOfferRepository $schoolOfferRepository,
        Kardex $kardex,
        SchoolPeriod $schoolPeriod,
        KardexRepository $kardexRepository
    ){
        $this->semester = $semester;
        $this->career = $career;
        $this->schoolOffer = $schoolOffer;
        $this->professorRepository = $professorRepository;
        $this->signature = $signature;
        $this->schoolShift = $schoolShift;
        $this->schoolGroup = $schoolGroup;
        $this->schoolOfferRepository =  $schoolOfferRepository;
        $this->kardex = $kardex;
        $this->schoolPeriod = $schoolPeriod;
        $this->kardexRepository = $kardexRepository;
        $this->middleware("auth");
    }

    public function schoolOffer(SchoolOfferRequest $request){
        $semesters = $this->semester->all();
        $careers = $this->career->all();
        $professors = $this->professorRepository->getActiveProfessors();
        $signatures = $this->signature->active()->get();
        $schoolShifts  = $this->schoolShift->all();
        $schoolGroups = $this->schoolGroup->all();
        if(!is_null($request->{'career-selected'})){
            $schoolOffers = $this->schoolOffer->with(["professor", "semester", "signature"])
                            ->where("career_id", $request->{"career-selected"})
                            ->where("semester_id", $request->{'semester-selected'})
                            ->where("school_shift_id", $request->{'school-shift-selected'})
                            ->where("school_group_id", $request->{"school-group-selected"})->get();
        }else{
            $schoolOffers = new Collection([]);
        }
        return view('admin.school-offer', compact('careers', 'semesters', 'schoolOffers', 'professors', 'signatures', 'schoolShifts', 'schoolGroups'));
    }

    public function storeSchoolOffer(StoreSchoolOfferRequest $request){
        $schoolOffer = $this->schoolOfferRepository->createSchoolOffer($request);
        return redirect()->back()->with("success", "Oferta escolar agregada correctamente");
    }

    public function updateSchoolOffer(SchoolOffer $schoolOffer, UpdateSchoolOfferRequest $request)
    {
        $schoolOffer = $this->schoolOfferRepository->updateSchoolOffer($schoolOffer,$request);
        return redirect()->back()->with("success", "Oferta escolar modificada correctamente");
    }

    public function list(){
        $kardexs = $this->kardex->paginate(25);
        $currentPeriod = $this->schoolPeriod->getCurrentSchoolPeriod();
        return view('admin.kardex.list', compact('kardexs','currentPeriod'));
    }

    public function search(Request $request){
        $data = [];

        if($request->has('q')){
            $search = $request->q;
            $data =Student::where('nombre','LIKE',"%$search%")
                    ->orWhere('paterno','LIKE',"%$search%")
                    ->orWhere('materno','LIKE',"%$search%")
                    ->orWhere('matricula_alumno','LIKE',"%$search%")
            		->limit(5)->get();
        }
        return response()->json($data);
    }

    public function searchPeriods(Request $request){
        $data = [];

        if($request->has('q')){
            $search = $request->q;
            $data =SchoolPeriod::where('code','LIKE',"%$search%")
            		->limit(5)->get();
        }
        return response()->json($data);
    }

    public function create(Student $student){

        $schoolOffers = $this->schoolOfferRepository->findSchoolOffersByStudent($student);
        $schoolPeriods = $this->schoolPeriod->orderBy("id", "desc")->get();
        $kardexs = $this->kardexRepository->findKardexByStudent($student);

        return view("admin.kardex.add", compact("schoolOffers", "student", "schoolPeriods", "kardexs"));
    }

    public function store(Student $student, KardexStoreRequest $request)
    {
        $kardexs = $this->kardexRepository->storeKardex($student, $request);

        if($kardexs instanceof \Throwable){
            return redirect()->back()->withErrors("Hubo un error al registrar, contacté con el administrador.");
        }

        return redirect()->back()->with("success", "Kardex registrado correctamente");
    }

    public function myKardex(Request $request){
        if($request->has("school-period")){
            $schoolPeriod = $this->kardexRepository->getPeriodWithKardex($request->{"school-period"});
        }else{
            $schoolPeriod = $this->kardexRepository->getPeriodWithKardex();
        }
        $student = auth()->user()->student;
        $schoolPeriods = $this->schoolPeriod->orderBy("created_at", "desc")->get();
        return view('kardex.student', compact('schoolPeriod', 'student', 'schoolPeriods'));
    }

    public function kardexQualification(Request $request)
    {
        $professor = auth()->user()->professor;
        $schoolPeriods = $this->schoolPeriod->orderBy("created_at", "desc")->get();
        $schoolPeriod = $this->kardexRepository->getPeriodWithKardexProfessor(request()->{"search-school-period"});
        $semesters = $this->semester->all();
        $schoolShifts = $this->schoolShift->all();
        $schoolGroups = $this->schoolGroup->all();
        $careers = $this->career->all();
        $signatures = $professor->signatures()->whereHas('kardexs', function($query) use($schoolPeriod){
            $query->where('period', $schoolPeriod->code);
        })->distinct()->get();

        if(request()->has("search-signature")){
            $kardexs = $this->kardex->where("matricula_professor", $professor->matricula_professor)->where("signature_id", request()->{"search-signature"})->where("period", request()->{"search-school-period"});
        }else{
            $kardexs = $this->kardex->where("matricula_professor", $professor->matricula_professor)->where("signature_id", @$signatures[0]->id_asignatura)->where("period", $schoolPeriod->code);
        }

        if(request()->has("search-career")){
            $kardexs = $kardexs->where("career_id",request()->{"search-career"});
        }

        if(request()->{"search-semester"} ){
            $kardexs = $kardexs->where("semester_id", request()->{"search-semester"});
        }else{
            $kardexs = $kardexs->where("semester_id", $this->semester->first()->id_semestre);
        }

        if(request()->{"search-school-shift"}){
            $kardexs = $kardexs->where("school_shift_id", request()->{"search-school-shift"});
        }

        if(request()->{"search-school-group"}){
            $kardexs = $kardexs->where("school_group_id", request()->{"search-school-group"});
        }
        $kardexs = $kardexs->get();
        return view('kardex.professor', compact('signatures', 'professor', 'careers', 'schoolPeriods','schoolPeriod', 'kardexs', 'semesters', 'schoolShifts', 'schoolGroups'));
    }

    public function searchSignatureByPeriod(SchoolPeriod $schoolPeriod){
        $signatures = auth()->user()->professor->signatures()->whereHas('kardexs', function($query) use($schoolPeriod){
            $query->where('period', $schoolPeriod->code);
        })->distinct()->get();
        return response()->json($signatures);
    }

    /**
     * save data from professor user
     */
    public function kardexQualificationSave(Kardex $kardex, KardexQualificationRequest $request){
        $kardex->prequalification = $request->prequalification;
        $kardex->assist_percent = $request->assist_percent;
        $kardex->approved_status = $request->approved_status;
        $kardex->save();

        $data = (object)[
            "quantity" => 1,
            "data" => $kardex,
            "status" => parent::SUCCESS_RESPONSE
        ];

        return response()->json($data, parent::SUCCESS_RESPONSE);
    }

    /**
     * get kardex information for get filled student kardex qualification
     */
    public function getKardexQualificationData(Kardex $kardex)
    {
        return response()->json($kardex, parent::SUCCESS_RESPONSE);
    }

    public function qualificationRequests(Request $request){
        $schoolPeriods = $this->schoolPeriod->orderBy("id", "desc")->get();
        $professors = $this->professorRepository->getActiveProfessors();
        if(request()->has('search-professors') && request()->has('search-period') ){
            $kardexs = $this->kardex->where("period", $request->{"search-period"})
                        ->where("matricula_professor", $request->{"search-professors"})->get();
        }else{
            $kardexs = $this->kardex->where("period", $schoolPeriods[0]->code)
            ->where("matricula_professor", $professors[0]->matricula_professor)->get();
        }
        return view('admin.kardex.qualification', compact('schoolPeriods', 'professors', 'kardexs'));

    }

    public function confirmQualification(Kardex $kardex, KardexConfirmRequest $request){
        $data_before = $kardex;
        if($request->{'kardex-qualification-status'} == "confirmado"){
            $kardex->qualification  = $kardex->prequalification;
            $kardex->kardex_qualification_status = $request->{"kardex-qualification-status"};
            $kardex->approved_type = $request->{"approved-type"};
            $kardex->approved_status = $request->{"approved-status"};
            $kardex->status_kardex = "Cursado";
            $kardex->save();
            $this->kardexRepository->kardexLog($data_before, $kardex);
            return redirect()->back()->with("success", "Estatus confirmado con éxito.");
        }else{
            $kardex->kardex_qualification_status = $request->{"kardex-qualification-status"};
            $this->kardexRepository->kardexLog($data_before, $kardex);
            return redirect()->back()->with("success", "Estatus rechazado.");
        }
    }


    public function sendProfessorConfirm(
        Career $career,
        Signature $signature,
        $period,
        Semester $semester,
        SchoolShift $schoolShift,
        SchoolGroup $schoolGroup
    ){

        $professor = auth()->user()->professor;
        $kardexInfo = $this->kardex->where("career_id", $career->id)
        ->where("semester_id", $semester->id_semestre)
        ->where("period", $period)->where("signature_id", $signature->id_asignatura)
        ->where("matricula_professor", $professor->matricula_professor)
        ->where("school_shift_id", $schoolShift->id_turno)
        ->where("school_group_id", $schoolGroup->id)
        ->where(function($query){
            return $query->where("kardex_qualification_status", "rechazado")
            ->orWhereNull("kardex_qualification_status");
        });
        $kardexConfirmation = new KardexConfirmation();
        $kardexConfirmation->request_number = $this->kardexRepository->generateRequestNumber();
        $kardexConfirmation->description = "Enviados a revisión";
        $kardexConfirmation->qualification_status = "revision";
        $kardexConfirmation->matricula_professor = $professor->matricula_professor;
        $kardexConfirmation->save();


        #todo crear function para generqr numero de solicitud para el apartado de control escolar
        $quantity = $kardexInfo->count();
        $kardexInfo->update([
            "kardex_qualification_status" => "revision",
            "kardex_confirmation_id" => $kardexConfirmation->id
        ]);
        $data = (object)[
            "quantity" => $quantity,
            "data" => $kardexConfirmation,
            "status" => parent::SUCCESS_RESPONSE
        ];

        return response()->json($data, parent::SUCCESS_RESPONSE);
    }

    public function qualificationRequestsList()
    {
        $kardexConfirmations = $this->kardexRepository->getOpenQualifications();
        return view('admin.kardex.qualification-requests', compact('kardexConfirmations'));
    }

    public function qualificationRequestsKardex(KardexConfirmation $kardexConfirmation)
    {
        return view("admin.kardex.qualification-request-kardex", compact('kardexConfirmation'));
    }

    public function SendQualificationRequestsKardex(Request $request){
        dd($request->all());
    }

    /**
     * @author Kareem Lorenzana
     * @desc create kardex by period using current school offer in current assignated career
     */
    public function dinamicCreateKardex(Request $request)
    {
        $kardexResponse = $this->kardexRepository->storeDinamicKardex();

        if($kardexResponse instanceof \Exception){
            return redirect()->back()->withErrors("Hubo un error al registrar, contacté con el administrador.");
        }

        return redirect()->back()->with("success", $kardexResponse->message);
    }

}
