<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CareerController;
use App\Http\Controllers\KardexController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\ProfessorController;
use App\Http\Controllers\SignatureController;
use App\Http\Controllers\Auth\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
    'register' => false
]);
Route::get('/', [HomeController::class, 'index'])->name('home');

Route::prefix('admin')->group(function(){
    Route::get('students', [UserController::class, 'students'])->middleware('role:Administrator|School Control')->name('admin.students');
    Route::resource('users', UserController::class)->names([
        'index' => 'admin.users.index',
        'create' => 'admin.users.create',
        'store' => 'admin.users.store',
        'edit' => 'admin.users.edit',
        'update' => 'admin.users.update'
    ])->middleware('role:Administrator|School Control');
    Route::post('users/verify-type', [UserController::class, 'verifyType'])->name('admin.users.verifyType');
    Route::post('users/change-status/{user}', [UserController::class, 'changeStatus'])->middleware('role:Administrator|School Control')->name('admin.user.change_status');
    Route::get('register', [UserController::class, 'register'])->middleware('role:Administrator|School Control')->name('admin.register');
    Route::get('school-offer', [KardexController::class, 'schoolOffer'])->name('admin.school-offer.list')->middleware('role:Administrator|School Control|Subdirection');
    Route::post('school-offer/store-school-offer', [KardexController::class, 'storeSchoolOffer'])->middleware('role:Administrator|School Control|Subdirection')->name('admin.school-offer.store');
    Route::get('school-offer/{schoolOffer}/edit', [KardexController::class, 'showSchoolOffer'])->middleware('role:Administrator|School Control|Subdirection')->name('admin.school-offer.edit');
    Route::put('school-offer/{schoolOffer}/edit', [KardexController::class, 'updateSchoolOffer'])->middleware('role:Administrator|School Control|Subdirection')->name('admin.school-offer.update');

    Route::resource('professors', ProfessorController::class)->names([
        'index' => 'admin.professors.index',
        'create' => 'admin.professors.create',
        'store' => 'admin.professors.store',
        'edit' => 'admin.professors.edit',
        'update' => 'admin.professors.update'
    ])->middleware('role:Administrator|School Control');

    Route::get('kardex/list', [KardexController::class, 'list'])->middleware('role:Administrator|School Control')->name('admin.kardex.list');
    Route::get('kardex/create/{student}', [KardexController::class, 'create'])->name('admin.kardex.create')->middleware('role:Administrator|School Control');
    Route::post('kardex/create/{student}', [KardexController::class, 'store'])->name('admin.kardex.store')->middleware('role:Administrator|School Control');
    //create dinamically school offer
    Route::post('kardex/dinamic-create', [KardexController::class, 'dinamicCreateKardex'])->name('admin.kardex.dinamic-create')->middleware('role:Administrator|School Control');

    Route::get('kardex/search', [KardexController::class, 'search'])->name('admin.kardex.search')->middleware('role:Administrator|School Control');
    Route::get('kardex/search-periods', [KardexController::class, 'searchPeriods'])->name('admin.kardex.search.periods')->middleware('role:Administrator|School Control');
    Route::get('kardex/qualification', [KardexController::class, 'qualificationRequests'])->name('admin.kardex.qualification')->middleware('role:Administrator|School Control');
    Route::put('kardex/confirm/{kardex}',  [KardexController::class, 'confirmQualification'])->name('admin.kardex.confirm-qualification')->middleware('role:Administrator|School Control');
    //qualifictations requests
    Route::get('kardex/qualification-requests', [KardexController::class, 'qualificationRequestsList'])->name('admin.kardex.qualification-requests')->middleware('role:Administrator|School Control');


    Route::get('kardex/qualification-requests/{kardexConfirmation}', [KardexController::class, 'qualificationRequestsKardex'])->name('admin.kardex.qualification-requests-kardex')->middleware('role:Administrator|School Control');
    //confirm o reject request in the school control side
    Route::post('kardex/qualification-requests/{kardexConfirmation}', [KardexController::class, 'SendQualificationRequestsKardex'])->name('admin.kardex.qualification-requests-kardex')->middleware('role:Administrator|School Control');


    Route::resource('signature', SignatureController::class)->names([
        'index' => 'admin.signature.index',
        'create' => 'admin.signature.create',
        'store' => 'admin.signature.store',
        'edit' => 'admin.signature.edit',
        'update' => 'admin.signature.update'
    ])->middleware('role:Administrator|School Control');
    Route::post('signature/toggle/{signature}', [SignatureController::class, "toggleStatus"])->name('admin.signature.toggle-status')->middleware('role:Administrator|School Control');

    //careers crud
    Route::resource('career', CareerController::class)->names([
        'index' => 'admin.career.index',
        'create' => 'admin.career.create',
        'store' => 'admin.career.store',
        'edit' => 'admin.career.edit',
        'update' => 'admin.career.update'
    ])->middleware('role:Administrator|School Control');

    Route::get('report-signature', [ReportController::class, 'reportSignature']);
    Route::get('reports/{period?}', [ReportController::class, 'listOfReports'])->name('admin.report.list')->middleware('role:Administrator|School Control');
    Route::get('report-signature-print/{career}/{signature}/{professor}/{schoolPeriod}/{semester}', [ReportController::class, 'printReport'])->name('admin.report.print-signature')->middleware('role:Administrator|School Control');
});

Route::get('my-kardex', [KardexController::class, 'myKardex'])->name('kardex')->middleware('role:Student');

Route::get('kardex-qualification', [KardexController::class, 'kardexQualification'])->name('kardex.professor')->middleware('role:Professor');
Route::get('kardex-qualification/{kardex}', [KardexController::class, 'getKardexQualificationData'])->name('kardex.professor.edit')->middleware('role:Professor');
Route::put('kardex-qualification/{kardex}', [KardexController::class, 'kardexQualificationSave'])->name('kardex.professor.update')->middleware('role:Professor');
Route::get('report-signature-print/{career}/{signature}/{professor}/{schoolPeriod}/{semester}/{schoolShift}/{schoolGroup}', [ReportController::class, 'printReport'])->name('report.professor.print-signature')->middleware('role:Administrator|School Control|Professor');

//ruta para confirmar calificaciones
Route::post('send-confirm-signature/{career}/{signature}/{schoolPeriod}/{semester}/{schoolShift}/{schoolGroup}', [KardexController::class, 'sendProfessorConfirm'])->name('kardex.professor.send-confirm')->middleware('role:Administrator|School Control|Professor');
